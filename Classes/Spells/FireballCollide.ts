/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class FireballCollide {
        game:Phaser.Game;
        states:Phaser.Sprite;

        constructor(game:Phaser.Game, x:number, y:number, z:number) {
            this.game = game;
            this.states = this.game.add.isoSprite(x, y, z - 16, "explosion", 0);

            this.states.animations.add('explosion', Phaser.Animation.generateFrameNames('explosion1_', 1, 90, '', 4), 60, false);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1);

            this.states.animations.play('explosion').onComplete.addOnce(()=> {
                this.states.destroy();
            });
        }
    }
}
