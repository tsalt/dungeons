/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class Quake {
        game: Phaser.Game;
        states;

        damage: number = 75;

        removeAfterImpact: boolean = false;
        alreadyDamaged = [];
        quakeSound;

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number, casterBodySize: number) {
            this.game = game;
            let quakeScale = 1.2;
            let quakeBodySize = casterBodySize * 4.5 / quakeScale;
            this.states = this.game.add.isoSprite(x + (casterBodySize - quakeBodySize * quakeScale) / 2, y + (casterBodySize - quakeBodySize * quakeScale) / 2, 0, "quake", 0, displayGroup);

            this.states.animations.add('quake', Phaser.Animation.generateFrameNames('sprite', 1, 6, ''), 15, false);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(quakeScale);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.collideWorldBounds = true;
            this.states.body.setSize(quakeBodySize, quakeBodySize, 64, 8, 8, 0);

            this.quakeSound = this.game.add.audio("quake");
            this.quakeSound.addMarker('quake', 0.4, 1, 1, false);
            this.quakeSound.play('quake');

            this.states.animations.play('quake').onComplete.addOnce(() => {
                this.states.body = null;
                this.states.destroy();
            });
        }

        update() {
        }
    }
}
