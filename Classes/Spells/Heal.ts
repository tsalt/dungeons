/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class Heal {
        game:Phaser.Game;
        states:Phaser.Sprite;

        constructor(game:Phaser.Game, x:number, y:number) {

            this.game = game;
            this.states = this.game.add.isoSprite(x, y, -8, "heal", 0);

            this.states.animations.add('heal', [0, 1, 2, 3, 4, 5], 12, false);

            this.states.anchor.setTo(0.5);

            this.states.animations.play('heal').onComplete.addOnce(()=> {
                this.states.destroy();
            });
        }
    }
}
