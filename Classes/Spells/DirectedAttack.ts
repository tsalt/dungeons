/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class DirectedAttack {
        game:Phaser.Game;
        states:Phaser.Sprite;

        posX:number;
        posY:number;

        alreadyDamaged = [];

        constructor(game:Phaser.Game, x:number, y:number, orientation:string, attackSize:number, casterBodySize:number) {
            this.game = game;

            this.setPosition(x, y, orientation, attackSize, casterBodySize);
        }

        setPosition(x:number, y:number, orientation:string, attackSize:number, casterBodySize:number) {
            if (orientation === "SE") {
                this.posX = x + casterBodySize;
                this.posY = y - (attackSize - casterBodySize) / 2;
            }
            else if (orientation === "S") {
                this.posX = x + casterBodySize * 0.8 - (attackSize - casterBodySize) / 2;
                this.posY = y + casterBodySize * 0.8 - (attackSize - casterBodySize) / 2;
            }
            else if (orientation === "SW") {
                this.posX = x - (attackSize - casterBodySize) / 2;
                this.posY = y + casterBodySize;
            }
            else if (orientation === "NW") {
                this.posX = x - attackSize;
                this.posY = y - (attackSize - casterBodySize) / 2;
            }
            else if (orientation === "N") {
                this.posX = x - casterBodySize * 0.8 - (attackSize - casterBodySize) / 2;
                this.posY = y - casterBodySize * 0.8 - (attackSize - casterBodySize) / 2;
            }
            else if (orientation === "NE") {
                this.posX = x - (attackSize - casterBodySize) / 2;
                this.posY = y - attackSize;
            }
            else if (orientation === "E") {
                this.posX = x + casterBodySize * 0.6 - (attackSize - casterBodySize) / 2;
                this.posY = y - casterBodySize * 0.6 - (attackSize - casterBodySize) / 2;
            }
            else if (orientation === "W") {
                this.posX = x - casterBodySize * 0.6 - (attackSize - casterBodySize) / 2;
                this.posY = y + casterBodySize * 0.6 - (attackSize - casterBodySize) / 2;
            }
        }
    }
}
