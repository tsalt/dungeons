/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class GoldDrop {
        game:Phaser.Game;
        states:Phaser.Sprite;

        pickEnabled:boolean = false;

        goldAmount:number;

        sound;

        constructor(game:Phaser.Game, displayGroup:Phaser.Group, x:number, y:number, zOffset:number, goldAmount:number) {
            this.game = game;
            this.goldAmount = goldAmount;

            let sprite;
            if (this.goldAmount <= 19) {
                sprite = "gold5";
            }
            else if (this.goldAmount > 20 && this.goldAmount < 60) {
                sprite = "gold25";
            }
            else {
                sprite = "gold50";
            }

            this.states = this.game.add.isoSprite(x, y, zOffset / 2, sprite, 0, displayGroup);
            this.states.animations.add('gold', [0, 1, 2, 3, 4, 5], 8, false);

            this.sound = this.game.add.audio('coinDrop');

            this.sound.addMarker('coinDrop', 1, 1, 1);

            this.sound.play("coinDrop");

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(24, 24, 16, 0, 0, -16);
            this.states.body.collideWorldBounds = true;

            this.states.animations.play('gold').onComplete.addOnce(()=> {
                this.pickEnabled = true;
            });
        }

        collectGold() {
            localStorage.setItem("gold", (parseInt(localStorage.getItem("gold")) + this.goldAmount).toString());
        }
    }
}
