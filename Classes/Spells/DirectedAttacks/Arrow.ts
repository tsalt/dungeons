/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../DirectedAttack.ts"/>

module Dungeons {

    export class Arrow extends DirectedAttack {
        orientation: string;

        bodySize: number = 24;
        bodyScale: number = 1;

        speed: number = 8;
        damage: number = 150;

        removeAfterImpact: boolean = true;
        // hasOnHitEffect: boolean = true;

        // fireballSound;
        // fireballCollideSound;

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number, orientation: string, casterBodySize: number) {
            super(game, x, y, orientation, 48, casterBodySize);

            this.orientation = orientation;

            this.setPosition(x, y, orientation, this.bodySize * this.bodyScale, casterBodySize);

            this.states = this.game.add.isoSprite(this.posX, this.posY, 0, "projectiles", 0, displayGroup);

            this.states.animations.add('fireArrowS', Phaser.Animation.generateFrameNames('sprite', 7, 7, ''), 10, true);
            this.states.animations.add('fireArrowSE', Phaser.Animation.generateFrameNames('sprite', 6, 6, ''), 10, true);
            this.states.animations.add('fireArrowE', Phaser.Animation.generateFrameNames('sprite', 5, 5, ''), 10, true);
            this.states.animations.add('fireArrowNE', Phaser.Animation.generateFrameNames('sprite', 4, 4, ''), 10, true);
            this.states.animations.add('fireArrowN', Phaser.Animation.generateFrameNames('sprite', 3, 3, ''), 10, true);
            this.states.animations.add('fireArrowNW', Phaser.Animation.generateFrameNames('sprite', 2, 2, ''), 10, true);
            this.states.animations.add('fireArrowW', Phaser.Animation.generateFrameNames('sprite', 1, 1, ''), 10, true);
            this.states.animations.add('fireArrowSW', Phaser.Animation.generateFrameNames('sprite', 8, 8, ''), 10, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(this.bodyScale);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(this.bodySize, this.bodySize, 24, 0, 0, 0);
            this.states.body.gravity = 0;

            this.states.animations.play('fireArrow' + this.orientation);
            this.states.lifespan = 1000;
            //
            // this.fireballSound = this.game.add.audio("fireball");
            // this.fireballSound.addMarker('fireball', 2, 2.8, 1, false);
            // this.fireballSound.play('fireball');
            //
            // this.fireballCollideSound = this.game.add.audio("fireballCollide");
            // this.fireballCollideSound.addMarker('fireballCollide', 1, 2, 2, false);
        }

        move() {
            if (this.orientation === "NW") {
                this.states.body.position.x -= this.speed;
            }
            else if (this.orientation === "SE") {
                this.states.body.position.x += this.speed;
            }
            else if (this.orientation === "NE") {
                this.states.body.position.y -= this.speed;
            }
            else if (this.orientation === "SW") {
                this.states.body.position.y += this.speed;
            }
            else if (this.orientation === "S") {
                this.states.body.position.x += this.speed * 0.8;
                this.states.body.position.y += this.speed * 0.8;
            }
            else if (this.orientation === "W") {
                this.states.body.position.x -= this.speed * 0.6;
                this.states.body.position.y += this.speed * 0.6;
            }
            else if (this.orientation === "E") {
                this.states.body.position.x += this.speed * 0.6;
                this.states.body.position.y -= this.speed * 0.6;
            }
            else if (this.orientation === "N") {
                this.states.body.position.x -= this.speed * 0.8;
                this.states.body.position.y -= this.speed * 0.8;
            }
        }

        update() {
            this.move();
        }
    }
}
