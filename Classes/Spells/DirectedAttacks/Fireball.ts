/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class Fireball extends DirectedAttack {
        orientation: string;

        bodySize: number = 24;
        bodyScale: number = 2;

        speed: number = 8;
        damage: number = 150;

        removeAfterImpact: boolean = true;
        hasOnHitEffect: boolean = true;

        fireballSound;
        fireballCollideSound;

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number, orientation: string, casterBodySize: number) {
            super(game, x, y, orientation, 48, casterBodySize);

            this.orientation = orientation;

            this.setPosition(x, y, orientation, this.bodySize * this.bodyScale, casterBodySize);

            this.states = this.game.add.isoSprite(this.posX, this.posY, 0, "fireball", 0, displayGroup);

            this.states.animations.add('fireballS', [48, 49, 50, 51, 52, 53, 54, 55], 30, true);
            this.states.animations.add('fireballSE', [40, 41, 42, 43, 44, 45, 46, 47], 30, true);
            this.states.animations.add('fireballE', [32, 33, 34, 35, 36, 37, 38, 39], 30, true);
            this.states.animations.add('fireballNE', [24, 25, 26, 27, 28, 29, 30, 31], 30, true);
            this.states.animations.add('fireballN', [16, 17, 18, 19, 20, 21, 22, 23], 30, true);
            this.states.animations.add('fireballNW', [8, 9, 10, 11, 12, 13, 14, 15], 30, true);
            this.states.animations.add('fireballW', [0, 1, 2, 3, 4, 5, 6, 7], 30, true);
            this.states.animations.add('fireballSW', [56, 57, 58, 59, 60, 61, 62, 63], 30, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(this.bodyScale);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(this.bodySize, this.bodySize, 24, 0, 0, 0);
            this.states.body.gravity = 0;

            this.states.animations.play('fireball' + this.orientation);
            this.states.lifespan = 1000;

            this.fireballSound = this.game.add.audio("fireball");
            this.fireballSound.addMarker('fireball', 0, 2, 1, false);
            this.fireballSound.play('fireball');

            this.fireballCollideSound = this.game.add.audio("fireballCollide");
            this.fireballCollideSound.addMarker('fireballCollide', 1, 2, 2, false);
        }

        move() {
            if (this.orientation === "NW") {
                this.states.body.position.x -= this.speed;
            }
            else if (this.orientation === "SE") {
                this.states.body.position.x += this.speed;
            }
            else if (this.orientation === "NE") {
                this.states.body.position.y -= this.speed;
            }
            else if (this.orientation === "SW") {
                this.states.body.position.y += this.speed;
            }
            else if (this.orientation === "S") {
                this.states.body.position.x += this.speed * 0.8;
                this.states.body.position.y += this.speed * 0.8;
            }
            else if (this.orientation === "W") {
                this.states.body.position.x -= this.speed * 0.6;
                this.states.body.position.y += this.speed * 0.6;
            }
            else if (this.orientation === "E") {
                this.states.body.position.x += this.speed * 0.6;
                this.states.body.position.y -= this.speed * 0.6;
            }
            else if (this.orientation === "N") {
                this.states.body.position.x -= this.speed * 0.8;
                this.states.body.position.y -= this.speed * 0.8;
            }
        }

        update() {
            this.move();
        }
    }
}
