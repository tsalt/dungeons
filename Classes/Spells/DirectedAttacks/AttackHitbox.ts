/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../DirectedAttack.ts"/>

module Dungeons {

    export class AttackHitbox extends DirectedAttack {

        constructor(game: Phaser.Game, x: number, y: number, orientation: string, attackSize: number, casterBodySize: number) {
            super(game, x, y, orientation, attackSize, casterBodySize);

            this.states = this.game.add.isoSprite(this.posX, this.posY, 0, "None", 0);
            this.states.animations.add('spawn', [0, 1, 2, 3, 4, 5], 8, false);

            this.states.anchor.setTo(0.5);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(attackSize, attackSize, 64, 0, 0, 0);
            this.states.body.collideWorldBounds = true;

            this.states.animations.play('spawn');
        }
    }
}
