/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>

module Dungeons {

    export class Blood {
        game:Phaser.Game;
        states:Phaser.Sprite;

        currentBlood = [];

        constructor(game:Phaser.Game, x:number, y:number, bodyHeight:number, zOffset:number) {
            this.game = game;

            this.currentBlood[0] = 'blood1';
            this.currentBlood[1] = 'blood2';
            this.currentBlood[2] = 'blood3';
            this.currentBlood[3] = 'blood4';

            let randomEffect = this.game.rnd.integerInRange(0, 3);

            this.states = this.game.add.isoSprite(x, y, zOffset / 2, this.currentBlood[randomEffect], 0);

            this.states.animations.add('spawnBlood', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 20, false);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(0.35 + bodyHeight / 108);

            this.states.animations.play('spawnBlood').onComplete.addOnce(()=> {
                this.states.destroy();
            });
        }
    }
}
