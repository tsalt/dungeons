/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="MapFloorConfig.ts"/>

/**
 * Draws isometric game map based on provided map config/s. The map may consist of several different floors that are on
 * top of each other.
 *
 * NOTE: According to the compiler there is/are error/s in this file. Ignore them! Everything is OK. This is because the
 * compiler doesn't know about the Isometric plugin and its types.
 *
 */
class MapManager {
    game:Phaser.Game;

    //The tile group that will hold the map tiles.
    floorGroup:Phaser.Group;

    floorCollection = [];

    //The count of the floors. The first floor is floor 0!
    currentFloor:number;

    constructor(gameRef:Phaser.Game) {
        this.game = gameRef;
        this.floorGroup = this.game.add.group();
    }

    /**
     * Adds new floor to the map by reading the provided floor config and drawing its tiles.
     * @param {MapFloorConfig} floorConf The config object for the current floor.
     */
    addFloor(floorConf:MapFloorConfig, floorNumber:number, bodyX?:number, bodyY?:number, bodyZ?:number,
             offSetX?:number, offSetY?:number, offSetZ?:number) {
        let confFileContentCacheKey:string = floorConf.configCacheKey;
        let spriteCacheKey:string = floorConf.tilesSetSpriteCacheKey;

        let tilesFrameWidth:number = floorConf.tilesSetFrameWidth;
        let tilesFrameHeight:number = floorConf.tilesSetFrameHeight;

        let offsetXTolerance:number = floorConf.tileSetFrameWidthTolerance;
        let offsetYTolerance:number = floorConf.tileSetFrameHeightTolerance;

        let floorTilesConfig:number[][] = this.getFloorConfig(confFileContentCacheKey);

        this.currentFloor = floorNumber;

        this.drawFloorMap(floorTilesConfig, spriteCacheKey, tilesFrameWidth, tilesFrameHeight, offsetXTolerance,
            offsetYTolerance, bodyX, bodyY, bodyZ, offSetX, offSetY, offSetZ);
    }

    /**
     * Returns 2D array representing the current floor tiles configuration.
     * @param  {string}   mapConfCacheKey The key for the already loaded file.
     * @return {number[][]} 2D array representing the current floor.
     */
    getFloorConfig(mapConfCacheKey:string):number[][] {
        let configFileContent = this.game.cache.getText(mapConfCacheKey);

        let mapRows:string[] = configFileContent.split("\n");

        let mapDimentions:number = mapRows.length;

        let tmpMapConf = new Array([]);

        for (let i = 0; i < mapDimentions; i++) {
            if (mapRows[i] == "") {
                mapDimentions--;
                break;
            }

            tmpMapConf[i] = mapRows[i].split(",");
        }

        for (let i = 0; i < mapDimentions; i++) {
            for (let j = 0; j < mapDimentions; j++) {
                tmpMapConf[i][j] = parseInt(tmpMapConf[i][j]);
            }
        }

        return tmpMapConf;
    }

    /**
     * Draws a map floor based on the provided config.
     * @param  {number[][]} tilesConfig            The tiles config for the floor.
     * @param  {string}     spriteCacheKey         The cache key of the sprite used for this floor.
     * @param  {number}     spriteFrameWidth       The with of each tile within the sprite.
     * @param  {number}     spriteFrameHeight      The height of each tile within the sprite.
     * @param  {number}     spriteOffsetXTolerance (Optional) X offset tolerance.
     * @param  {number}     spriteOffsetYTolerance (Optional) Y offset tolerance.
     */
    drawFloorMap(tilesConfig:number[][], spriteCacheKey:string, spriteFrameWidth:number, spriteFrameHeight:number,
                 spriteOffsetXTolerance?:number, spriteOffsetYTolerance?:number, bodyX?:number, bodyY?:number,
                 bodyZ?:number, offSetX?:number, offSetY?:number, offSetZ?:number) {

        let mapDimentions = tilesConfig.length;

        let spriteOffsetX:number = spriteFrameWidth/2;
        let spriteOffsetY:number = spriteFrameHeight / 2;

        if (spriteOffsetXTolerance) {
            spriteOffsetX += spriteOffsetXTolerance;
        }
        if (spriteOffsetYTolerance) {
            spriteOffsetY += spriteOffsetYTolerance;
        }

        let floorTile;

        for (let i = 0; i < mapDimentions; i++) {
            for (let j = 0; j < mapDimentions; j++) {
                // -1 means no tile on the current position.
                if (tilesConfig[j][i] == -1) {
                    continue;
                }

                floorTile = this.game.add.isoSprite(i * spriteOffsetX, j * spriteOffsetY, this.currentFloor,
                    spriteCacheKey, tilesConfig[j][i], this.floorGroup);
                floorTile.anchor.set(0.5);

                this.floorCollection.push(floorTile);

                this.game.physics.isoArcade.enable(floorTile);
                floorTile.body.collideWorldBounds = false;
                floorTile.body.immovable = true;
                floorTile.body.moves = false;
                floorTile.body.setSize(bodyX, bodyY, bodyZ, offSetX, offSetY, offSetZ);
            }
        }
    }
}