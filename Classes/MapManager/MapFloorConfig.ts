/**
 * Instances of this class are provided to MapManager as a config for a map floor.
 */
class MapFloorConfig {

    //The key of already loaded config file content.

    configCacheKey: string;
    tilesSetSpriteCacheKey: string;
    tilesSetFrameWidth: number;
    tilesSetFrameHeight: number;

    //Additional tile offset on the X coordinate.
    tileSetFrameWidthTolerance: number;
    //Additional tile offset on the Y coordinate.
    tileSetFrameHeightTolerance: number;

    constructor(confCacheKey?: string, tilesSetCacheKey?: string, tileFrameWidth?: number, tileFrameHeight?: number,
                tileFrameWidthTolerance?: number, tileFrameHeightTolerance?: number) {
        this.configCacheKey = confCacheKey;
        this.tilesSetSpriteCacheKey = tilesSetCacheKey;
        this.tilesSetFrameWidth = tileFrameWidth;
        this.tilesSetFrameHeight = tileFrameHeight;
        this.tileSetFrameWidthTolerance = tileFrameWidthTolerance;
        this.tileSetFrameHeightTolerance = tileFrameHeightTolerance;
    }
}