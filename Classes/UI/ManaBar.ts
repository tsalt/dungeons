/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Units/Hero.ts"/>

module Dungeons {
    export class ManaBar {

        game:Phaser.Game;
        emptyHpBar;
        fullHpBar;
        text;
        scale;

        heroCurrentMana:number;
        heroMaxMana:number;

        constructor(game:Phaser.Game) {
            this.game = game;

            this.scale = 0.7;
            this.fullHpBar = this.game.add.sprite(this.game.width * 0.5, this.game.height - 75, "manaBar");
            this.fullHpBar.scale.set(this.scale);
            this.fullHpBar.anchor.set(0.5);
            this.emptyHpBar = this.game.add.sprite(this.game.width * 0.5, this.game.height - 75, "emptyHpBar");
            this.emptyHpBar.scale.set(this.scale);
            this.emptyHpBar.anchor.set(0.5);

            let style = {font: "13px Arial", fill: "#ffffff"};
            this.text = this.game.add.text(this.game.width * 0.5, this.game.height - 70, "", style);
            this.text.anchor.set(0.5);
            this.text.alpha = 1;
            this.text.stroke = "rgba(0,0,0,0.6)";
            this.text.strokeThickness = 3;

            this.fullHpBar.fixedToCamera = true;
            this.emptyHpBar.fixedToCamera = true;
            this.text.fixedToCamera = true;
        }

        setCrop() {
            let cropRect = new Phaser.Rectangle(0, 0, (this.heroCurrentMana / this.heroMaxMana) * this.emptyHpBar.width * (1 / this.scale), this.emptyHpBar.height * (1 / this.scale));
            this.fullHpBar.crop(cropRect);

            if (this.heroCurrentMana >= 0) {
                this.text.setText(this.heroCurrentMana + "%");
            }
            else {
                this.text.setText(0 + "%");
            }

            this.fullHpBar.updateCrop();
        }

        update(hero:Hero) {
            this.heroCurrentMana = hero.currentMana;
            this.heroMaxMana = hero.maxMana;
            this.setCrop();
        }
    }
}