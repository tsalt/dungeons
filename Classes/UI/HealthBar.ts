/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../Lib/phaser/phaser.d.ts"/>

module Dungeons {
    export class HealthBar {

        game:Phaser.Game;
        emptyHpBar;
        fullHpBar;
        text;

        constructor(game:Phaser.Game) {
            this.game = game;

            this.emptyHpBar = this.game.add.sprite(0, 0, "emptyFloatingBar");
            this.emptyHpBar.scale.set(0.4, 0.5);
            this.fullHpBar = this.game.add.sprite(0, 0, "fullFloatingBar");
            this.fullHpBar.scale.set(0.4, 0.5);

            let style = {font: "11px Arial", fill: "#ffffff"};
            this.text = this.game.add.text(0, 0, "", style);
            this.text.anchor.set(0.5);
            this.text.alpha = 0.8;
        }

        update(health:number, maxHealth:number, x:number, y:number) {
            this.fullHpBar.position.set(x - this.emptyHpBar.width * 0.5, y - this.emptyHpBar.height + 10);
            this.text.position.set(x, y);

            let cropRect = new Phaser.Rectangle(0, 0,(health / maxHealth) * this.emptyHpBar.width*2.5, this.emptyHpBar.height*2);
            this.fullHpBar.crop(cropRect);

            if (health >= 0) {
                this.text.setText(health + " / " + maxHealth + " \n/ " + Math.round(x) + " / " + Math.round(y));
            }
            else {
                this.text.setText(0 + " / " + maxHealth);
            }

            this.fullHpBar.updateCrop();
            this.emptyHpBar.position.set(x - this.emptyHpBar.width * 0.5, y - this.emptyHpBar.height + 10);
        }
    }
}
