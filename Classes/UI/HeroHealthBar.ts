/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Units/Hero.ts"/>

module Dungeons {
    export class HeroHealthBar {

        game:Phaser.Game;
        emptyHpBar;
        fullHpBar;
        text;

        heroCurrentHealth:number;
        heroMaxHealth:number;

        constructor(game:Phaser.Game) {
            this.game = game;

            this.emptyHpBar = this.game.add.sprite(this.game.width * 0.5 - 150, this.game.height - 50, "emptyHpBar");
            this.emptyHpBar.scale.set(1);
            this.fullHpBar = this.game.add.sprite(this.game.width * 0.5 - 150, this.game.height - 50, "fullHpBar");
            this.fullHpBar.scale.set(1);

            let style = {font: "15px Arial", fill: "#ffffff"};
            this.text = this.game.add.text(this.game.width * 0.5, this.game.height -8, "", style);
            this.text.anchor.set(0.5, 1);
            this.text.alpha = 1;
            this.text.stroke = "rgba(0,0,0,0.6)";
            this.text.strokeThickness = 3;

            this.fullHpBar.fixedToCamera = true;
            this.emptyHpBar.fixedToCamera = true;
            this.text.fixedToCamera = true;
        }

        setCrop() {
            let cropRect = new Phaser.Rectangle(0, 0, (this.heroCurrentHealth / this.heroMaxHealth) * this.emptyHpBar.width, this.emptyHpBar.height);
            this.fullHpBar.crop(cropRect);

            if (this.heroCurrentHealth >= 0) {
                this.text.setText(this.heroCurrentHealth + " / " + this.heroMaxHealth);
            }
            else {
                this.text.setText(0 + " / " + this.heroMaxHealth);
            }

            this.fullHpBar.updateCrop();
        }

        update(hero:Hero){
            this.heroCurrentHealth = hero.currentHealth;
            this.heroMaxHealth = hero.maxHealth;
            this.setCrop();
        }
    }
}