/// <reference path="../../Lib/phaser/phaser.d.ts"/>

module Dungeons {
    export class Joystick extends Phaser.Plugin {
        game:Phaser.Game;

        private imageGroup:Phaser.Sprite[] = [];
        private initialPoint:Phaser.Point;
        private doUpdate:boolean = false;
        private pointer:Phaser.Pointer;

        ////Public Properties
        public gamepadMode:boolean = true;
        public settings:{

            maxDistanceInPixels:number;
            singleDirection:boolean;
            //Sets the pad to follow the finger movement
            float: boolean;
        };

        public cursors:{
            N:boolean;
            NE:boolean;
            NW:boolean;
            S:boolean;
            SE:boolean;
            SW:boolean;
            W:boolean;
            E:boolean;
        };

        constructor(game:Phaser.Game, gamepadMode:boolean = true) {
            super(game, new PIXI.DisplayObject());
            this.game = game;
            this.gamepadMode = gamepadMode;
            this.pointer = this.game.input.pointer1;

            //Setup the images
            this.imageGroup.push(this.game.add.sprite(0, 0, 'joystick_base'));
            this.imageGroup.push(this.game.add.sprite(0, 0, 'joystick_segment'));
            this.imageGroup.push(this.game.add.sprite(0, 0, 'joystick_knob'));

            this.imageGroup.forEach(function (e) {
                e.anchor.set(0.5);
                e.visible = false;
                e.fixedToCamera = true;
                e.scale.set(0.5);
            });

            //Setup Default Settings
            this.settings = {
                maxDistanceInPixels: 60,
                singleDirection: false,
                float: true,
            };

            //Setup Default State
            this.cursors = {
                N: false,
                NE: false,
                NW: false,
                S: false,
                SE: false,
                SW: false,
                W: false,
                E: false
            };

            this.inputEnable();
        }

        public inputEnable():void {
            this.game.input.onDown.add(this.createStick, this);
            this.game.input.onUp.add(this.removeStick, this);
            this.active = true;
        }

        private inSector(pointer:Phaser.Pointer):boolean {
            if (pointer.position.x < this.game.width / 1.5)
                return true;
        }

        private createStick(pointer:Phaser.Pointer):void {
            if (!this.inSector(pointer)) return;
            //Else update the pointer (it may be the first touch)
            this.pointer = pointer;

            this.imageGroup.forEach(function (e) {
                e.visible = true;
                e.bringToTop();

                e.cameraOffset.x = this.pointer.worldX;
                e.cameraOffset.y = this.pointer.worldY;

            }, this);

            //Allow updates on the stick while the screen is being touched
            this.doUpdate = true;

            //Start the Stick on the position that is being touched right now
            this.initialPoint = this.pointer.position.clone();
        }

        private removeStick(pointer:Phaser.Pointer):void {
            if (pointer.id != this.pointer.id) return;

            //Deny updates on the stick
            this.doUpdate = false;

            this.imageGroup.forEach(function(e){
                e.visible = false;
            });

            this.cursors.S = false;
            this.cursors.SE = false;
            this.cursors.SW = false;
            this.cursors.W = false;
            this.cursors.E = false;
            this.cursors.NW = false;
            this.cursors.NE = false;
            this.cursors.N = false;
        }

        preUpdate() {
            if (this.doUpdate) {
                this.setDirection();
            }
        }

        setDirection():void {
            let d = this.initialPoint.distance(this.pointer.position);
            let maxDistanceInPixels = this.settings.maxDistanceInPixels;

            let deltaX = this.pointer.position.x - this.initialPoint.x;
            let deltaY = this.pointer.position.y - this.initialPoint.y;

            let angle = this.initialPoint.angle(this.pointer.position);

            if (d > maxDistanceInPixels) {

                deltaX = Math.cos(angle) * maxDistanceInPixels;
                deltaY = Math.sin(angle) * maxDistanceInPixels;

                if (this.settings.float) {
                    this.initialPoint.x = this.pointer.x - deltaX;
                    this.initialPoint.y = this.pointer.y - deltaY;
                }
            }

            this.cursors.N = (deltaY <= -40);
            this.cursors.NE = (deltaY <= 0 && deltaX >= 20 && deltaX <= 50);
            this.cursors.E = (deltaX >= 40);
            this.cursors.SE = (deltaY >= 20 && deltaY <= 50 && deltaX >= 0);
            this.cursors.S = (deltaY >= 50 );
            this.cursors.SW = (deltaY <= 50 && deltaY >= 20 && deltaX <= 0);
            this.cursors.W = (deltaX <= -40);
            this.cursors.NW = (deltaY <= -20 && deltaY >= -50 && deltaX <= 0);

            this.imageGroup.forEach(function (e, i) {
                e.cameraOffset.x = this.initialPoint.x + (deltaX) * i / (this.imageGroup.length - 1);
                e.cameraOffset.y = this.initialPoint.y + (deltaY) * i / (this.imageGroup.length - 1);
            }, this);
        }
    }
}
