/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="SomeMenu.ts"/>
/// <reference path="ActionButton.ts"/>
/// <reference path="HeroHealthBar.ts"/>
///<reference path="ManaBar.ts"/>

module Dungeons {
    export class UIManager {

        game: Phaser.Game;
        isPaused: boolean;

        fadedBackground: Phaser.Image;
        buttonPlatform: Phaser.Image;

        someMenuButton: Phaser.Button;
        someMenu: SomeMenu;

        attackButton: ActionButton;
        fireballButton: ActionButton;
        quakeButton: ActionButton;
        blockButton: ActionButton;
        potionButton: ActionButton;

        currentGoldText;

        hpBar: HeroHealthBar;
        manaBar: ManaBar;
        hero;

        constructor(game: Phaser.Game, hero: Hero) {
            this.game = game;
            this.hero = hero;

            this.isPaused = false;

            // Paused imitation background
            this.fadedBackground = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "blackScreen");
            this.fadedBackground.anchor.setTo(0.5);
            this.fadedBackground.alpha = 0;
            this.fadedBackground.fixedToCamera = true;

            this.someMenu = new SomeMenu(this.game);

            // Button platform
            this.buttonPlatform = this.game.add.image(1280, 0, 'buttonPlatform');
            this.buttonPlatform.fixedToCamera = true;
            this.buttonPlatform.scale.set(1, 0.7);
            this.buttonPlatform.angle = 90;

            // Gold
            let goldIcon = this.game.add.image(990, 45, 'goldIcon');
            goldIcon.anchor.set(0.5);
            goldIcon.scale.set(-0.8, 0.7);
            goldIcon.fixedToCamera = true;

            let style = {font: '25pt Arial', fill: 'white'};
            this.currentGoldText = this.game.add.text(1090, 55, localStorage.getItem("gold"), style);
            this.currentGoldText.stroke = 'black';
            this.currentGoldText.strokeThickness = 2;
            this.currentGoldText.anchor.set(0.5);
            this.currentGoldText.fixedToCamera = true;

            // Buttons on screen
            this.someMenuButton = this.game.add.button(1235, 50, 'menuButton', (function () {
                this.Open(this.someMenu)
            }), this);
            this.someMenuButton.anchor.setTo(0.5);
            this.someMenuButton.scale.setTo(0.8);
            this.someMenuButton.fixedToCamera = true;

            //Buttons for the hero
            this.attackButton = new ActionButton(this.game, 1110, 550, 'attackOrb', this.hero, "attack", 0, 0, 1.4, false, true);
            this.fireballButton = new ActionButton(this.game, 980, 580, 'flameOrb', this.hero, "castFireball",
                this.hero.fireballCooldown, this.hero.fireballCost, 1, false, true);
            this.quakeButton = new ActionButton(this.game, 1040, 460, 'bombOrb', this.hero, "castQuake", this.hero.quakeCooldown,
                this.hero.quakeCost, 1, false, true);
            this.potionButton = new ActionButton(this.game, 1120, 320, 'redPotion', this.hero, "potion", 0, 0, 1.3, true, false);

            this.hpBar = new HeroHealthBar(this.game);
            this.manaBar = new ManaBar(this.game);
        }

        update() {
            this.hpBar.update(this.hero);
            this.manaBar.update(this.hero);
            this.currentGoldText.text = localStorage.getItem("gold");
            this.attackButton.update();
            this.fireballButton.update();
            this.quakeButton.update();
            this.potionButton.update();

            if (!this.someMenu.isShown) {
                this.fadedBackground.alpha = 0;

                this.attackButton.inputEnabled = true;
                this.fireballButton.inputEnabled = true;
                this.quakeButton.inputEnabled = true;
                this.potionButton.inputEnabled = true;

                this.isPaused = false;
            }

            if (this.game.input.keyboard.isDown(Phaser.Keyboard.F)) {
                this.hero.castFireball(this.fireballButton);
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.G)) {
                this.hero.castQuake(this.quakeButton);
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
                if (this.hero.checkUnitAction()) {
                    this.hero.attack();
                }
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.H)) {
                if (localStorage.getItem("potions") != "0") {
                    localStorage.setItem("potions", (parseInt(localStorage.getItem("potions")) - 1).toString());
                    this.hero.heal();
                    this.potionButton.labelText = localStorage.getItem("potions");
                }
            }
        }

        Open(element: any) {
            this.fadedBackground.alpha = 0.75;
            this.fadedBackground.bringToTop();
            element.Show();

            this.attackButton.inputEnabled = false;
            this.fireballButton.inputEnabled = false;
            this.quakeButton.inputEnabled = false;
            this.potionButton.inputEnabled = false;

            this.isPaused = true;
        }
    }
}