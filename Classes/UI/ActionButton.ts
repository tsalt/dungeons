/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Units/Hero.ts"/>

module Dungeons {
    export class ActionButton extends Phaser.Button {
        game: Phaser.Game;
        labelText: Phaser.Text;
        cooldown: number;
        timer: Phaser.Timer;
        originalHeight;
        hero: Hero;
        cost;
        isOnCooldown;

        constructor(game: Phaser.Game, x: number, y: number, key: string, hero: Hero, action: string,
                    cooldown: number, cost: number, scale: number, label: boolean, hasBackground: boolean) {
            super(game, x, y);
            this.game = game;
            this.cooldown = cooldown;
            this.hero = hero;
            this.cost = cost;
            super(this.game, x, y, key, () => {
                if (action === "castFireball") {
                    hero.castFireball(this);
                }
                else if (action === "castQuake") {
                    hero.castQuake(this);
                }
                else if (action === "attack") {
                    hero.attack();
                }
                else if (action === "potion" && localStorage.getItem("potions") != "0") {
                    localStorage.setItem("potions", (parseInt(localStorage.getItem("potions")) - 1).toString());
                    hero.heal();
                }
            }, this);

            this.scale.set(scale);
            this.input.priorityID = 1;
            this.fixedToCamera = true;
            this.game.add.existing(this);
            this.hitArea = new Phaser.Circle((this.width / 2) / scale, (this.height / 2) / scale, 100);
            this.originalHeight = this.height;

            if (hasBackground) {
                let image = this.game.add.image(x, y, "buttonBackground");
                image.scale.set(scale);
                image.fixedToCamera = true;
            }

            this.bringToTop();

            if (label) {
                let textStyle = {fill: "#8B1914"};
                this.labelText = this.game.add.text(x + 95, y + 35, localStorage.getItem("potions"), textStyle);
                this.labelText.fontSize = 50;
                this.labelText.strokeThickness = 3;
                this.labelText.stroke = "#FFF";
                this.labelText.fixedToCamera = true;
                this.labelText.bringToTop();
            }
        }

        startCooldown(cooldown: number) {
            this.isOnCooldown = true;
            this.alpha = 0.5;
            this.timer = this.game.time.create(true);
            this.timer.add(Phaser.Timer.SECOND * cooldown, this.resetCooldown, this);
            this.timer.start();
        }

        resetCooldown() {
            this.isOnCooldown = false;
            this.alpha = 1;
        }

        update() {
            if (this.labelText) {
                this.labelText.text = localStorage.getItem("potions");
            }
            if (this.timer) {
                let cropRect = new Phaser.Rectangle(0, 0, this.width, ( this.timer.seconds / this.cooldown) * (this.originalHeight));
                this.crop(cropRect);
                this.updateCrop();
            }
            if (this.hero.currentMana >= this.cost) {
                this.alpha = 1;
            }
            else {
                this.alpha = 0.5;
            }
        }
    }
}