/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../Lib/phaser/phaser.d.ts"/>

module Dungeons {
    export class SomeMenu {

        game:Phaser.Game;
        isShown:boolean;

        menuBackground:Phaser.Image;
        resumeButton:Phaser.Button;

        pausedText;
        resumeText;

        constructor(game:Phaser.Game) {
            this.game = game;

            this.isShown = false;

            this.menuBackground = this.game.add.image(this.game.width * 0.5, this.game.height * 0.45, "popupWindow");
            this.menuBackground.scale.setTo(1.1);
            this.menuBackground.anchor.setTo(0.5);
            this.menuBackground.fixedToCamera = true;
            this.menuBackground.visible = false;

            this.resumeButton = this.game.add.button(this.game.width * 0.5, 425, 'button2', this.Hide, this, 1, 2, 0);
            this.resumeButton.anchor.setTo(0.5);
            this.resumeButton.scale.setTo(0.9);
            this.resumeButton.fixedToCamera = true;
            this.resumeButton.visible = false;

            let titleStyle = {font: '60px scochflashfont', fill: 'white'};
            this.pausedText = this.game.add.text(this.game.width * 0.5, 250, "PAUSED", titleStyle);
            this.pausedText.stroke = 'black';
            this.pausedText.strokeThickness = 3;
            this.pausedText.anchor.set(0.5);
            this.pausedText.fixedToCamera = true;
            this.pausedText.visible = false;

            let style = {font: '30px scochflashfont', fill: 'white'};
            this.resumeText = this.game.add.text(this.game.width * 0.5, 425, "Resume", style);
            this.resumeText.stroke = 'black';
            this.resumeText.strokeThickness = 3;
            this.resumeText.anchor.set(0.5);
            this.resumeText.fixedToCamera = true;
            this.resumeText.visible = false;
        }

        Show() {
            this.menuBackground.visible = true;
            this.menuBackground.bringToTop();
            this.resumeButton.visible = true;
            this.resumeButton.bringToTop();
            this.pausedText.visible = true;
            this.pausedText.bringToTop();
            this.resumeText.visible = true;
            this.resumeText.bringToTop();

            this.isShown = true;
        }

        Hide() {
            this.menuBackground.visible = false;
            this.resumeButton.visible = false;
            this.pausedText.visible = false;
            this.resumeText.visible = false;

            this.isShown = false;
        }
    }
}