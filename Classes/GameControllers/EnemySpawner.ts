/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
///<reference path="../Units/Enemies/Ant.ts"/>
///<reference path="../Units/Enemies/Minotaur.ts"/>
///<reference path="../Units/Enemies/MudGolem.ts"/>
///<reference path="../Units/Enemy.ts"/>
///<reference path="../Units/Enemies/Werebear.ts"/>
///<reference path="../Units/Enemies/Orc.ts"/>
///<reference path="../Units/Enemies/SkeletonKnight.ts"/>
///<reference path="../Units/Enemies/Zombie.ts"/>
///<reference path="../Units/Enemies/Archer.ts"/>
///<reference path="../MapManager/MapManager.ts"/>

module Dungeons {

    export class EnemySpawner {
        game;
        mapManager: MapManager;

        spawnTimer;
        spawnInterval = 6;

        enemies = [];

        constructor(game: Phaser.Game, enemies, mapManager) {
            this.game = game;
            this.enemies = enemies;
            this.mapManager = mapManager;

            this.spawnTimer = this.game.time.create(false);
            this.spawnTimer.start();

            this.spawnEnemy();
        }

        spawnEnemy() {
            this.spawnTimer.add(Phaser.Timer.SECOND * this.spawnInterval, this.spawnEnemy, this);

            if (this.enemies.length <= 10) {
                let randomX = this.game.rnd.integerInRange(0, 1800);
                let randomY = this.game.rnd.integerInRange(0, 1800);

                let spawnAnimation = this.game.add.isoSprite(randomX, randomY, 0, "spawn", 0);
                spawnAnimation.animations.add('spawn', [0, 1, 2, 3], 4, false);

                spawnAnimation.anchor.setTo(0.5);

                spawnAnimation.animations.play('spawn').onComplete.addOnce(() => {
                    spawnAnimation.destroy();

                    switch (this.game.rnd.integerInRange(1, 8)) {
                        case 1:
                            this.enemies.push(new Ant(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 2:
                            this.enemies.push(new MudGolem(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 4:
                            this.enemies.push(new Orc(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 3:
                            this.enemies.push(new Zombie(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 5:
                            this.enemies.push(new Werebear(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 6:
                            this.enemies.push(new Minotaur(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 7:
                            this.enemies.push(new SkeletonKnight(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                        case 8:
                            this.enemies.push(new Archer(this.game, this.mapManager.floorGroup, randomX, randomY));
                            break;
                    }
                });
            }
        }
    }
}