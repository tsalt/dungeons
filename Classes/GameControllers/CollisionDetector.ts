/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
///<reference path="../Units/Hero.ts"/>
///<reference path="../MapManager/MapManager.ts"/>
///<reference path="../Spells/FireballCollide.ts"/>
///<reference path="../Spells/Blood.ts"/>

module Dungeons {

    export class CollisionDetector {
        game;
        mapManager: MapManager;

        enemies = [];

        pickUpItems;
        hero: Hero;

        constructor(game: Phaser.Game, enemies, mapManager, pickUpItems, hero) {
            this.game = game;
            this.enemies = enemies;
            this.hero = hero;
            this.mapManager = mapManager;
            this.pickUpItems = pickUpItems;
        }

        collisionChecks() {
            this.mapManager.floorCollection.forEach(function (mapCollision) {
                // Hero against mapCollision
                this.game.physics.isoArcade.collide(this.hero.states[0], mapCollision);

                // Hero's spells against mapCollision
                this.hero.spellCollection.forEach(function (spell) {
                    if (this.game.physics.isoArcade.overlap(spell.states, mapCollision)) {
                        if (spell.removeAfterImpact) {
                            spell.fireballSound.stop();
                            spell.fireballCollideSound.play("fireballCollide");
                            spell.states.kill();
                        }
                        if (spell.hasOnHitEffect) {
                            new FireballCollide(this.game, mapCollision.body.x, mapCollision.body.y, spell.states.body.height / 2);
                        }
                    }
                }, this);

                // Enemies against mapCollision
                this.enemies.forEach(function (enemy) {
                    this.game.physics.isoArcade.collide(enemy.states, mapCollision);
                }, this);
            }, this);

            this.enemies.forEach(function (enemy) {
                // Enemies against Hero
                this.game.physics.isoArcade.collide(enemy.states, this.hero.states[0]);

                // Enemies against Hero's attack
                if (this.hero.attackHitbox && enemy.currentState != UnitStates.DYING) {
                    if (this.hero.attackHitbox.alreadyDamaged.indexOf(enemy.states) === -1 &&
                        this.game.physics.isoArcade.overlap(this.hero.attackHitbox.states, enemy.states)) {
                        enemy.currentHealth -= this.hero.damage;
                        this.hero.addMana();
                        new Blood(this.game, enemy.states.body.x, enemy.states.body.y, enemy.states.body.height, enemy.states.body.z);
                        this.hero.attackHitbox.alreadyDamaged.push(enemy.states);
                    }
                }
                //Enemies against Hero's spells
                this.hero.spellCollection.forEach(function (spell) {
                    if (this.game.physics.isoArcade.overlap(spell.states, enemy.states)) {
                        if (spell.removeAfterImpact) {
                            spell.fireballSound.stop();
                            spell.fireballCollideSound.play("fireballCollide");
                            spell.states.kill();
                            enemy.currentHealth -= spell.damage;
                        }
                        else {
                            if (spell.alreadyDamaged.indexOf(enemy.states) === -1) {
                                spell.alreadyDamaged.push(enemy.states);
                                enemy.currentHealth -= spell.damage;
                            }
                        }

                        if (spell.hasOnHitEffect) {
                            let fireballCollide = new FireballCollide(this.game,
                                enemy.states.body.x, enemy.states.body.y, spell.states.body.height / 2)
                        }
                    }
                }, this);

                //Enemies' attacks against Hero
                if (enemy.attackHitbox) {
                    if (enemy.attackHitbox.alreadyDamaged.indexOf(this.hero.states[0]) === -1 &&
                        this.game.physics.isoArcade.overlap(this.hero.states[0], enemy.attackHitbox.states)) {
                        this.hero.currentHealth -= enemy.damage;
                        new Blood(this.game, this.hero.states[0].body.x, this.hero.states[0].body.y,
                            this.hero.states[0].body.height, this.hero.states[0].body.z);
                        enemy.attackHitbox.alreadyDamaged.push(this.hero.states[0]);
                    }
                }
            }, this);

            // Hero against pickUp
            this.pickUpItems.forEach(function (item) {
                if (this.game.physics.isoArcade.overlap(this.hero.states[0], item.states)) {
                    if (item.pickEnabled) {
                        item.collectGold();
                        this.collectedGold += item.goldAmount;
                        this.pickUpItems.splice(this.pickUpItems.indexOf(item), 1);
                        item.states.destroy();
                    }
                }
            }, this);
        }
    }
}