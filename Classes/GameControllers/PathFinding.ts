/// <reference path="../../Lib/phaser/phaser.d.ts"/>
///<reference path="../Units/Hero.ts"/>
///<reference path="../MapManager/MapManager.ts"/>

module Dungeons {

    export class PathFinding {
        pathfindingPlugin;
        enemyDirection: string;

        currentHeroXTile;
        currentHeroYtile;

        constructor(mapManager) {
            // Creating AI
            this.pathfindingPlugin = new EasyStar.js();
            let level: number[][] = mapManager.getFloorConfig("level1_1");
            this.pathfindingPlugin.setGrid(level);
            this.pathfindingPlugin.setAcceptableTiles([-1]);
            this.pathfindingPlugin.enableDiagonals();
        }

        pathfinding(enemy) {
            let currentEnemyXtile = Math.floor(enemy.states.body.position.x / 32);
            let currentEnemyYtile = Math.floor(enemy.states.body.position.y / 32);
            let currentNextPointX;
            let currentNextPointY;

            this.pathfindingPlugin.findPath(currentEnemyXtile, currentEnemyYtile,
                this.currentHeroXTile, this.currentHeroYtile, (path) => {
                    if (path === null) {
                        //console.log("The path to the destination point was not found.");
                        this.enemyDirection = "idle";
                    }
                    if (path) {
                        if (typeof path[1] != "undefined") {
                            currentNextPointX = path[1].x;
                            currentNextPointY = path[1].y;
                        }
                    }

                    if (!(currentNextPointX === this.currentHeroXTile && currentNextPointY === this.currentHeroYtile)) {
                        if (currentNextPointX < currentEnemyXtile && currentNextPointY < currentEnemyYtile) {
                            this.enemyDirection = "NW";
                        }
                        else if (currentNextPointX == currentEnemyXtile && currentNextPointY < currentEnemyYtile) {
                            this.enemyDirection = "N";
                        }
                        else if (currentNextPointX > currentEnemyXtile && currentNextPointY < currentEnemyYtile) {
                            this.enemyDirection = "NE";
                        }
                        else if (currentNextPointX < currentEnemyXtile && currentNextPointY == currentEnemyYtile) {
                            this.enemyDirection = "W";
                        }
                        else if (currentNextPointX > currentEnemyXtile && currentNextPointY == currentEnemyYtile) {
                            this.enemyDirection = "E";
                        }
                        else if (currentNextPointX > currentEnemyXtile && currentNextPointY > currentEnemyYtile) {
                            this.enemyDirection = "SE";
                        }
                        else if (currentNextPointX == currentEnemyXtile && currentNextPointY > currentEnemyYtile) {
                            this.enemyDirection = "S";
                        }
                        else if (currentNextPointX < currentEnemyXtile && currentNextPointY > currentEnemyYtile) {
                            this.enemyDirection = "SW";
                        }
                    }
                    else if (currentNextPointX - 30 <= this.currentHeroXTile && currentNextPointY - 30 <= this.currentHeroYtile) {
                        this.enemyDirection = "fireArrow";
                    }
                    else {
                        this.enemyDirection = "attack";
                    }
                    enemy.update(this.enemyDirection);
                });
            this.pathfindingPlugin.calculate();
        }
    }
}