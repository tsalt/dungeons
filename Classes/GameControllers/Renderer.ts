/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
///<reference path="../Units/Enemies/Ant.ts"/>
///<reference path="../Units/Enemies/Minotaur.ts"/>
///<reference path="../Units/Enemies/MudGolem.ts"/>
///<reference path="../Units/Enemy.ts"/>
///<reference path="../Units/Enemies/Werebear.ts"/>
///<reference path="../Units/Enemies/Orc.ts"/>
///<reference path="../Units/Enemies/SkeletonKnight.ts"/>
///<reference path="../Units/Enemies/Zombie.ts"/>
///<reference path="../MapManager/MapManager.ts"/>

module Dungeons {

    export class Renderer {
        game;
        mapManager:MapManager;

        enemies = [];
        hero;

        pickUpItems;

        constructor(game: Phaser.Game, enemies, mapManager, hero, pickUpItems) {
            this.game = game;
            this.enemies = enemies;
            this.mapManager = mapManager;
            this.hero = hero;
            this.pickUpItems = pickUpItems;
            this.render();
        }

        render() {
            this.game.debug.text(this.game.time.fps.toString() || '--', 5, 14, "#FFFFFF");

            this.mapManager.floorGroup.forEach(function (cube) {
                if (cube.inCamera) {
                    this.game.debug.body(cube, 'rgba(189, 221, 235, 0.6)', false);
                }
            }, this);

            // this.enemies.forEach(function (enemy) {
            //     if (enemy.attackHitbox) {
            //         this.game.debug.body(enemy.attackHitbox.states, 'rgba(255, 0, 0, 0.6)', false);
            //     }
            // }, this);
            //
            // if (this.hero.spellCollection) {
            //     this.hero.spellCollection.forEach(function (spell) {
            //         this.game.debug.body(spell.states, 'rgba(255, 0, 0, 0.6)', false);
            //     }, this);
            // }
            //
            // if (this.hero.attackHitbox) {
            //     this.game.debug.body(this.hero.attackHitbox.states, 'rgba(255, 0, 0, 0.6)', false);
            // }

            // this.pickUpItems.forEach(function (cube) {
            //     this.game.debug.body(cube.states, 'rgba(189, 221, 235, 0.6)', false);
            // }, this);
        }
    }
}