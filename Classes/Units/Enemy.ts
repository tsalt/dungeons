/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../UI/HealthBar.ts"/>
///<reference path="../Spells/DirectedAttacks/Arrow.ts"/>
///<reference path="../Spells/DirectedAttacks/AttackHitbox.ts"/>
///<reference path="../Spells/GoldDrop.ts"/>
///<reference path="Unit.ts"/>

module Dungeons {

    export class Enemy extends Unit {
        states: Phaser.Sprite;

        hpBar: HealthBar;

        currentHealth: number;
        maxHealth: number;
        damage: number;

        goldDropAmount: number;

        spellCollection = [];

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, currentHealth: number,
                    maxHealth: number, speed: number, attackSize: number, damage: number) {
            super(game, displayGroup, attackSize, speed);

            this.currentHealth = currentHealth;
            this.maxHealth = maxHealth;
            this.speed = speed;
            this.attackSize = attackSize;
            this.damage = damage;

            this.currentState = UnitStates.IDLE;

            this.idleAnimation = "idleW";

            // Creating floating health bar
            this.hpBar = new HealthBar(this.game);
        }

        // cast() {
        //     this.currentState = UnitStates.CASTING;
        //     this.states.animations.play('castSpell' + this.orientation).onComplete.addOnce(() => {
        //         this.currentState = UnitStates.IDLE;
        //     });
        // }

        itemDrop(array: any) {
            array.push(new GoldDrop(this.game, this.displayGroup, this.states.body.x, this.states.body.y, this.states.body.z, this.goldDropAmount));
        }

        fireArrow() {
            this.currentState = UnitStates.CASTING;
            this.states.animations.play('fireArrow' + this.orientation).onComplete.addOnce(() => {
                let arrow = new Arrow(this.game, this.displayGroup, this.states.body.position.x,
                    this.states.body.position.y, this.orientation, this.states.body.widthX);
                this.spellCollection.push(arrow);
                this.currentState = UnitStates.IDLE;
            });
        }

        update(key: string) {
            if (this.currentState != UnitStates.DYING) {

                if (this.currentState != UnitStates.ATTACKING && this.currentState != UnitStates.CASTING) {
                    if (key === "idle") {
                        this.states.animations.play(this.idleAnimation);
                    }
                    else if (key === "attack") {
                        super.attack(this.states);
                    }
                    else if (key == "fireArrow") {
                        super.attack(this.states);
                        //this.fireArrow();
                    }
                    else {
                        super.move(key, this.states);
                    }
                }

                if (this.currentHealth > 0) {
                    this.hpBar.update(this.currentHealth, this.maxHealth, this.states.position.x, this.states.position.y - this.states.body.height);
                }
                else if (this.hpBar) {
                    this.hpBar.emptyHpBar.destroy();
                    this.hpBar.fullHpBar.destroy();
                    this.hpBar.text.destroy();
                    this.hpBar = null;
                    this.dying(this.states);
                }
            }
        }
    }
}
