/// <reference path="../../Lib/phaser/phaser.d.ts"/>

module Dungeons {

    export class Model {
        game:Phaser.Game;

        positionX;
        positionY;

        states = [];
        armor = [];
        weapon = [];
        head = [];
        shield = [];

        chosenHead;
        chosenArmor;
        chosenShield;
        chosenWeapon;

        idleAnimations = [];
        idleAnimation;

        constructor(game:Phaser.Game, positionX:number, positionY:number, chosenHead:number, chosenArmor:number, chosenWeapon:number, chosenShield:number) {
            this.game = game;
            this.chosenArmor = chosenArmor;
            this.chosenHead = chosenHead;
            this.chosenWeapon = chosenWeapon;
            this.chosenShield = chosenShield;
            this.positionX = positionX;
            this.positionY = positionY;

            this.head[0] = 'Short Hair';
            this.head[1] = 'Hood';
            this.head[2] = 'Bald';

            this.armor[0] = 'Cloth';
            this.armor[1] = 'Leather';
            this.armor[2] = 'Plate';

            this.shield[0] = 'None';
            this.shield[1] = 'Buckler';
            this.shield[2] = 'Shield';

            this.weapon[0] = 'Rod';
            this.weapon[1] = 'Staff';
            this.weapon[2] = 'Great Staff';
            this.weapon[3] = 'Dagger';
            this.weapon[4] = 'Short Sword';
            this.weapon[5] = 'Long Sword';
            this.weapon[6] = 'Great Sword';

            this.states[0] = this.game.add.sprite(this.positionX, this.positionY, this.armor[this.chosenArmor], 0);
            this.states[1] = this.game.add.sprite(this.positionX, this.positionY, this.head[this.chosenHead], 0);
            this.states[2] = this.game.add.sprite(this.positionX, this.positionY, this.weapon[this.chosenWeapon], 0);
            this.states[3] = this.game.add.sprite(this.positionX, this.positionY, this.shield[this.chosenShield], 0);

            for (let i = 0; i < 4; i++) {
                this.states[i].animations.add('idleS', [192, 193, 194, 195], 5, true);
                this.states[i].animations.add('idleSE', [160, 161, 162, 163], 5, true);
                this.states[i].animations.add('idleE', [128, 129, 130, 131], 5, true);
                this.states[i].animations.add('idleNE', [96, 97, 98, 99], 5, true);
                this.states[i].animations.add('idleN', [64, 65, 66, 67], 5, true);
                this.states[i].animations.add('idleNW', [32, 33, 34, 35], 5, true);
                this.states[i].animations.add('idleW', [0, 1, 2, 3], 5, true);
                this.states[i].animations.add('idleSW', [224, 225, 226, 227], 5, true);
            }

            this.idleAnimations[0] = 'idleS';
            this.idleAnimations[1] = 'idleSE';
            this.idleAnimations[2] = 'idleE';
            this.idleAnimations[3] = 'idleNE';
            this.idleAnimations[4] = 'idleN';
            this.idleAnimations[5] = 'idleNW';
            this.idleAnimations[6] = 'idleW';
            this.idleAnimations[7] = 'idleSW';


            for (let i = 0; i < 4; i++) {
                this.states[i].anchor.setTo(0.5);
                this.states[i].scale.setTo(2.2);

                this.states[i].animations.play(this.idleAnimations[0]);
            }
        }
    }
}
