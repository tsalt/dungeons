/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../UI/Joystick.ts"/>
/// <reference path="../Spells/Quake.ts"/>
/// <reference path="../Spells/Heal.ts"/>
///<reference path="../Spells/DirectedAttacks/Fireball.ts"/>
///<reference path="../Spells/DirectedAttacks/AttackHitbox.ts"/>
///<reference path="../UI/HeroHealthBar.ts"/>
///<reference path="Unit.ts"/>

module Dungeons {

    export class Hero extends Unit {
        states = [];
        joystick: Joystick;

        spellCollection = [];

        fireballCooldown: number = 2;
        quakeCooldown: number = 3;

        currentHealth = 600;
        maxHealth = 600;
        currentMana = 100;
        maxMana = 100;
        damage: number = 25;

        attackSound;

        fireballCost: number = 15;
        quakeCost: number = 30;

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, chosenHead: string, chosenArmor: string, chosenWeapon: string, chosenShield: string) {
            super(game,displayGroup, 40, 4);
            this.game = game;

            this.currentState = UnitStates.IDLE;
            this.joystick = new Joystick(this.game, true);
            this.game.add.plugin(this.joystick);

            this.dieSound = this.game.add.audio("playerDeath");
            this.dieSound.addMarker("die", 0, 2, 1, false);

            this.states[0] = this.game.add.isoSprite(200, 700, 0, chosenArmor, 0, displayGroup);
            this.states[1] = this.game.add.isoSprite(200, 700, 0, chosenHead, 0, displayGroup);
            this.states[2] = this.game.add.isoSprite(200, 700, 0, chosenWeapon, 0, displayGroup);
            this.states[3] = this.game.add.isoSprite(200, 700, 0, chosenShield, 0, displayGroup);

            for (let i = 0; i < 4; i++) {
                this.states[i].animations.add('runS', [196, 197, 198, 199, 200, 201, 202, 203], 10, true);
                this.states[i].animations.add('runSE', [164, 165, 166, 167, 168, 169, 170, 171], 10, true);
                this.states[i].animations.add('runE', [132, 133, 134, 135, 136, 137, 138, 139], 10, true);
                this.states[i].animations.add('runNE', [100, 101, 102, 103, 104, 105, 106, 107], 10, true);
                this.states[i].animations.add('runN', [68, 69, 70, 71, 72, 73, 74, 75], 10, true);
                this.states[i].animations.add('runNW', [36, 37, 38, 39, 40, 41, 42, 43], 10, true);
                this.states[i].animations.add('runW', [4, 5, 6, 7, 8, 9, 10, 11], 10, true);
                this.states[i].animations.add('runSW', [228, 229, 230, 231, 232, 233, 234, 235], 10, true);

                this.states[i].animations.add('attackS', [204, 205, 206, 207], 7, false);
                this.states[i].animations.add('attackSE', [172, 173, 174, 175], 7, false);
                this.states[i].animations.add('attackE', [140, 141, 142, 143], 7, false);
                this.states[i].animations.add('attackNE', [108, 109, 110, 111], 7, false);
                this.states[i].animations.add('attackN', [76, 77, 78, 79], 7, false);
                this.states[i].animations.add('attackNW', [44, 45, 46, 47], 7, false);
                this.states[i].animations.add('attackW', [12, 13, 14, 15], 7, false);
                this.states[i].animations.add('attackSW', [236, 237, 238, 239], 7, false);

                this.states[i].animations.add('dieS', [210, 211, 212, 213, 214, 215], 5, false);
                this.states[i].animations.add('dieSE', [178, 179, 180, 181, 182, 183], 5, false);
                this.states[i].animations.add('dieE', [146, 147, 148, 149, 150, 151], 5, false);
                this.states[i].animations.add('dieNE', [114, 115, 116, 117, 118, 119], 5, false);
                this.states[i].animations.add('dieN', [82, 83, 84, 85, 86, 87], 5, false);
                this.states[i].animations.add('dieNW', [50, 51, 52, 53, 54, 55], 5, false);
                this.states[i].animations.add('dieW', [18, 19, 20, 21, 22, 23], 5, false);
                this.states[i].animations.add('dieSW', [242, 243, 244, 245, 246, 247], 5, false);

                this.states[i].animations.add('blockS', [208, 209], 5, false);
                this.states[i].animations.add('blockSE', [176, 177], 5, false);
                this.states[i].animations.add('blockE', [144, 145], 5, false);
                this.states[i].animations.add('blockNE', [112, 113], 5, false);
                this.states[i].animations.add('blockN', [80, 81], 5, false);
                this.states[i].animations.add('blockNW', [48, 49], 5, false);
                this.states[i].animations.add('blockW', [16, 17], 5, false);
                this.states[i].animations.add('blockSW', [240, 241], 5, false);

                this.states[i].animations.add('castSpellS', [216, 217, 218, 219], 10, false);
                this.states[i].animations.add('castSpellSE', [184, 185, 186, 187], 10, false);
                this.states[i].animations.add('castSpellE', [152, 153, 154, 155], 10, false);
                this.states[i].animations.add('castSpellNE', [120, 121, 122, 123], 10, false);
                this.states[i].animations.add('castSpellN', [88, 89, 90, 91], 10, false);
                this.states[i].animations.add('castSpellNW', [56, 57, 58, 59], 10, false);
                this.states[i].animations.add('castSpellW', [24, 25, 26, 27], 10, false);
                this.states[i].animations.add('castSpellSW', [248, 249, 250, 251], 10, false);

                this.states[i].animations.add('idleS', [192, 193, 194, 195], 5, true);
                this.states[i].animations.add('idleSE', [160, 161, 162, 163], 5, true);
                this.states[i].animations.add('idleE', [128, 129, 130, 131], 5, true);
                this.states[i].animations.add('idleNE', [96, 97, 98, 99], 5, true);
                this.states[i].animations.add('idleN', [64, 65, 66, 67], 5, true);
                this.states[i].animations.add('idleNW', [32, 33, 34, 35], 5, true);
                this.states[i].animations.add('idleW', [0, 1, 2, 3], 5, true);
                this.states[i].animations.add('idleSW', [224, 225, 226, 227], 5, true);
            }
            this.idleAnimation = "idle" + this.orientation;

            for (let i = 0; i < 4; i++) {
                this.states[i].anchor.setTo(0.5);
                this.states[i].scale.setTo(1.2);
                this.game.physics.isoArcade.enable(this.states[i]);
                this.states[i].body.collideWorldBounds = true;
                this.states[i].body.setSize(32, 32, 48, 0, 0, -16);
            }

            this.attackSound = this.game.add.audio("heroAttack");
            this.attackSound.addMarker("heroAttack", 0, 1, 1, false);
        }

        attack() {
            super.attack(this.states[0]);
            this.attackSound.play('heroAttack');
        }

        addMana() {
            if (this.currentMana < this.maxMana) {
                if (this.currentMana + 10 > this.maxMana) {
                    this.currentMana = this.maxMana;
                }
                else {
                    this.currentMana += 10;
                }
            }
        }

        heal() {
            let potionSound = this.game.add.audio("drinkPotion");
            potionSound.addMarker("drinkPotion", 0, 1, 0.5, false);
            potionSound.play("drinkPotion");
            this.currentState = UnitStates.BLOCKING;
            this.states[0].animations.play('block' + this.orientation).onComplete.addOnce(() => {
                let heal = new Heal(this.game, this.states[0].body.position.x, this.states[0].body.position.y);
                this.currentHealth += 100;
                this.currentState = UnitStates.IDLE;
            })
        }

        castQuake(quakeButton: ActionButton) {
            this.castSpell(this.quakeCost, quakeButton, "quake");
        }

        castFireball(fireballButton: ActionButton) {
            this.castSpell(this.fireballCost, fireballButton, "fireball");
        }

        castSpell(spellCost: number, spellButton: ActionButton, spellType: string) {
            if (this.checkUnitAction()) {
                if (spellCost <= this.currentMana && !spellButton.isOnCooldown) {
                    this.currentMana -= this.quakeCost;
                    spellButton.startCooldown(this.quakeCooldown);

                    this.currentState = UnitStates.CASTING;
                    this.states[0].animations.play('castSpell' + this.orientation).onComplete.addOnce(() => {
                        if (spellType == "quake") {
                            let quake = new Quake(this.game, this.displayGroup, this.states[0].body.position.x,
                                this.states[0].body.position.y, this.states[0].body.widthX);
                            this.spellCollection.push(quake);
                        }
                        else if (spellType == "fireball") {
                            let fireball = new Fireball(this.game, this.displayGroup, this.states[0].body.position.x,
                                this.states[0].body.position.y, this.orientation, this.states[0].body.widthX);
                            this.spellCollection.push(fireball);
                        }
                        this.currentState = UnitStates.IDLE;
                    })
                }
            }
        }

        update() {
            if (this.currentState != UnitStates.DYING) {
                if (this.currentState != UnitStates.ATTACKING && this.currentState != UnitStates.CASTING
                    && this.currentState != UnitStates.BLOCKING) {
                    if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_7) || this.joystick.cursors.NW) {
                        super.move("NW", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_3) || this.joystick.cursors.SE) {
                        super.move("SE", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_9) || this.joystick.cursors.NE) {
                        super.move("NE", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_1) || this.joystick.cursors.SW) {
                        super.move("SW", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_2) || this.joystick.cursors.S) {
                        super.move("S", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_4) || this.joystick.cursors.W) {
                        super.move("W", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_6) || this.joystick.cursors.E) {
                        super.move("E", this.states[0]);
                    }
                    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.NUMPAD_8) || this.joystick.cursors.N) {
                        super.move("N", this.states[0]);
                    }

                    else {
                        this.currentState = UnitStates.IDLE;
                        this.states[0].animations.play(this.idleAnimation);
                        this.states[0].body.immovable = true;
                    }
                }

                if (this.currentHealth <= 0) {
                    this.dying(this.states[0]);
                }

                // Sync position and animation
                for (let i = 1; i < 4; i++) {
                    this.states[i].animations.play(this.states[0].animations.name);
                    this.states[i].body.position = this.states[0].body.position;
                    this.states[i].body.immovable = this.states[0].body.immovable;
                }
            }
        }
    }
}