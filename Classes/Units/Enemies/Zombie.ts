/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class Zombie extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 240, 240, 1.5, 42, 25);
            this.goldDropAmount = this.game.rnd.integerInRange(5, 70);

            this.dieSound = this.game.add.sound("zombieDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            this.states = this.game.add.isoSprite(x, y, 0, "zombie", 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 221, 228, ''), 6, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 185, 192, ''), 6, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 149, 156, ''), 6, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 113, 120, ''), 6, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 77, 84, ''), 6, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 41, 48, ''), 6, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 6, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 257, 264, ''), 6, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 229, 238, ''), 8, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 193, 202, ''), 8, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 157, 166, ''), 8, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 121, 130, ''), 8, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 85, 94, ''), 8, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 49, 58, ''), 8, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 13, 22, ''), 8, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 265, 274, ''), 8, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 245, 252, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 209, 216, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 173, 180, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 137, 144, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 101, 108, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 65, 72, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 29, 36, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 281, 288, ''), 10, false);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 217, 220, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 181, 184, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 145, 148, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 109, 112, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 73, 76, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 37, 40, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 253, 256, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1.3);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.collideWorldBounds = true;
            this.states.body.setSize(24, 24, 32, 0, 0, -24);
        }
    }
}
