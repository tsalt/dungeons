/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class Werebear extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 200, 200, 3.7, 48, 20);

            this.goldDropAmount = this.game.rnd.integerInRange(5, 80);

            this.dieSound = this.game.add.sound("werebearDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            this.states = this.game.add.isoSprite(x, y, 0, "werebear", 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 10, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 33, 40, ''), 10, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 61, 68, ''), 10, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 89, 96, ''), 10, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 117, 124, ''), 10, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 145, 152, ''), 10, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 173, 180, ''), 10, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 201, 208), 10, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 13, 16, ''), 5, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 41, 44, ''), 5, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 69, 72, ''), 5, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 97, 100, ''), 5, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 125, 128, ''), 5, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 153, 156, ''), 5, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 181, 184, ''), 5, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 209, 212, ''), 5, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 17, 24, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 45, 52, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 73, 80, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 101, 108, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 129, 136, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 157, 164, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 185, 192, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 213, 220, ''), 10, false);

            this.states.animations.add('castSpellS', Phaser.Animation.generateFrameNames('sprite', 25, 28, ''), 5, true);
            this.states.animations.add('castSpellSE', Phaser.Animation.generateFrameNames('sprite', 53, 56, ''), 5, true);
            this.states.animations.add('castSpellE', Phaser.Animation.generateFrameNames('sprite', 81, 84, ''), 5, true);
            this.states.animations.add('castSpellNE', Phaser.Animation.generateFrameNames('sprite', 109, 112, ''), 5, true);
            this.states.animations.add('castSpellN', Phaser.Animation.generateFrameNames('sprite', 137, 140, ''), 5, true);
            this.states.animations.add('castSpellNW', Phaser.Animation.generateFrameNames('sprite', 165, 168, ''), 5, true);
            this.states.animations.add('castSpellW', Phaser.Animation.generateFrameNames('sprite', 193, 196, ''), 5, true);
            this.states.animations.add('castSpellSW', Phaser.Animation.generateFrameNames('sprite', 221, 224, ''), 5, true);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 29, 32, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 57, 60, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 85, 88, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 113, 116, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 141, 144, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 169, 172, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 197, 200, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(40, 40, 56, 0, 0, -8);
            this.states.body.collideWorldBounds = true;
        }
    }
}
