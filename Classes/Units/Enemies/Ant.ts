/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class Ant extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 150, 150, 5, 48, 15);

            this.goldDropAmount = this.game.rnd.integerInRange(5, 50);

            this.dieSound = this.game.add.sound("antDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            let typeOfAnt = [];
            typeOfAnt[0] = 'fireAnt';
            typeOfAnt[1] = 'blackAnt';
            typeOfAnt[2] = 'iceAnt';

            this.states = this.game.add.isoSprite(x, y, 0, typeOfAnt[this.game.rnd.integerInRange(0, 2)], 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 197, 204, ''), 10, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 165, 172, ''), 10, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 133, 140, ''), 10, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 101, 108, ''), 10, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 69, 76, ''), 10, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 37, 44, ''), 10, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 10, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 229, 236, ''), 10, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 205, 210, ''), 5, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 173, 178, ''), 5, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 141, 146, ''), 5, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 109, 114, ''), 5, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 77, 82, ''), 5, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 45, 50, ''), 5, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 13, 18, ''), 5, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 237, 242, ''), 5, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 211, 224, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 179, 192, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 147, 160, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 115, 128, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 83, 96, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 51, 64, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 19, 32, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 243, 256, ''), 10, false);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 193, 196, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 161, 164, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 129, 132, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 97, 100, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 65, 68, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 33, 36, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 225, 228, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.collideWorldBounds = true;
            this.states.body.setSize(40, 40, 32, 0, 0, -16);
        }
    }
}
