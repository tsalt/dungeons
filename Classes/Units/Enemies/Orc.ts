/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class Orc extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 225, 225, 3.6, 44, 25);

            this.goldDropAmount = this.game.rnd.integerInRange(5, 80);

            this.dieSound = this.game.add.sound("orcDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            this.states = this.game.add.isoSprite(x, y, 0, "orc", 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 197, 204, ''), 10, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 165, 172, ''), 10, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 133, 140, ''), 10, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 101, 108, ''), 10, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 69, 76, ''), 10, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 37, 44, ''), 10, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 10, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 229, 236, ''), 10, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 205, 208, ''), 5, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 173, 176, ''), 5, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 141, 144, ''), 5, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 109, 112, ''), 5, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 77, 80, ''), 5, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 45, 48, ''), 5, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 13, 16, ''), 5, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 237, 240, ''), 5, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 211, 216, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 179, 184, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 147, 152, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 115, 120, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 83, 88, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 51, 56, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 19, 24, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 243, 248, ''), 10, false);

            this.states.animations.add('castSpellS', Phaser.Animation.generateFrameNames('sprite', 217, 220, ''), 5, true);
            this.states.animations.add('castSpellSE', Phaser.Animation.generateFrameNames('sprite', 185, 188, ''), 5, true);
            this.states.animations.add('castSpellE', Phaser.Animation.generateFrameNames('sprite', 153, 156, ''), 5, true);
            this.states.animations.add('castSpellNE', Phaser.Animation.generateFrameNames('sprite', 121, 124, ''), 5, true);
            this.states.animations.add('castSpellN', Phaser.Animation.generateFrameNames('sprite', 89, 92, ''), 5, true);
            this.states.animations.add('castSpellNW', Phaser.Animation.generateFrameNames('sprite', 51, 54, ''), 5, true);
            this.states.animations.add('castSpellW', Phaser.Animation.generateFrameNames('sprite', 25, 28, ''), 5, true);
            this.states.animations.add('castSpellSW', Phaser.Animation.generateFrameNames('sprite', 249, 252, ''), 5, true);

            this.states.animations.add('blockS', Phaser.Animation.generateFrameNames('sprite', 209, 210, ''), 5, true);
            this.states.animations.add('blockSE', Phaser.Animation.generateFrameNames('sprite', 177, 178, ''), 5, true);
            this.states.animations.add('blockE', Phaser.Animation.generateFrameNames('sprite', 145, 146, ''), 5, true);
            this.states.animations.add('blockNE', Phaser.Animation.generateFrameNames('sprite', 113, 114, ''), 5, true);
            this.states.animations.add('blockN', Phaser.Animation.generateFrameNames('sprite', 81, 82, ''), 5, true);
            this.states.animations.add('blockNW', Phaser.Animation.generateFrameNames('sprite', 49, 50, ''), 5, true);
            this.states.animations.add('blockW', Phaser.Animation.generateFrameNames('sprite', 17, 18, ''), 5, true);
            this.states.animations.add('blockSW', Phaser.Animation.generateFrameNames('sprite', 241, 242, ''), 5, true);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 193, 196, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 161, 164, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 129, 132, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 97, 100, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 65, 68, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 33, 36, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 225, 228, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1.4);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(28, 28, 40, 0, 0, -24);
            this.states.body.collideWorldBounds = true;
        }
    }
}
