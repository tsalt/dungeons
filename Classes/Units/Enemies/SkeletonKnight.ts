/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class SkeletonKnight extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 250, 250, 3.3, 42, 25);

            this.goldDropAmount = this.game.rnd.integerInRange(10, 80);

            this.dieSound = this.game.add.sound("skeletonKnightDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            this.states = this.game.add.isoSprite(x, y, 0, "skeletonKnight", 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 173, 180, ''), 10, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 145, 152, ''), 10, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 117, 124, ''), 10, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 89, 96, ''), 10, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 61, 68, ''), 10, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 33, 40, ''), 10, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 10, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 201, 208, ''), 10, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 181, 184, ''), 5, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 153, 156, ''), 5, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 125, 128, ''), 5, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 97, 100, ''), 5, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 69, 72, ''), 5, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 41, 44, ''), 5, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 13, 16, ''), 5, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 209, 212, ''), 5, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 191, 196, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 163, 168, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 135, 140, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 107, 112, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 79, 84, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 51, 56, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 23, 28, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 219, 224, ''), 10, false);

            this.states.animations.add('castSpellS', Phaser.Animation.generateFrameNames('sprite', 185, 188, ''), 5, true);
            this.states.animations.add('castSpellSE', Phaser.Animation.generateFrameNames('sprite', 157, 160, ''), 5, true);
            this.states.animations.add('castSpellE', Phaser.Animation.generateFrameNames('sprite', 129, 132, ''), 5, true);
            this.states.animations.add('castSpellNE', Phaser.Animation.generateFrameNames('sprite', 101, 104, ''), 5, true);
            this.states.animations.add('castSpellN', Phaser.Animation.generateFrameNames('sprite', 73, 76, ''), 5, true);
            this.states.animations.add('castSpellNW', Phaser.Animation.generateFrameNames('sprite', 45, 48, ''), 5, true);
            this.states.animations.add('castSpellW', Phaser.Animation.generateFrameNames('sprite', 17, 20, ''), 5, true);
            this.states.animations.add('castSpellSW', Phaser.Animation.generateFrameNames('sprite', 213, 216, ''), 5, true);

            this.states.animations.add('blockS', Phaser.Animation.generateFrameNames('sprite', 189, 190, ''), 5, true);
            this.states.animations.add('blockSE', Phaser.Animation.generateFrameNames('sprite', 161, 162, ''), 5, true);
            this.states.animations.add('blockE', Phaser.Animation.generateFrameNames('sprite', 133, 134, ''), 5, true);
            this.states.animations.add('blockNE', Phaser.Animation.generateFrameNames('sprite', 105, 106, ''), 5, true);
            this.states.animations.add('blockN', Phaser.Animation.generateFrameNames('sprite', 77, 78, ''), 5, true);
            this.states.animations.add('blockNW', Phaser.Animation.generateFrameNames('sprite', 49, 50, ''), 5, true);
            this.states.animations.add('blockW', Phaser.Animation.generateFrameNames('sprite', 21, 22, ''), 5, true);
            this.states.animations.add('blockSW', Phaser.Animation.generateFrameNames('sprite', 217, 218, ''), 5, true);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 169, 172, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 141, 144, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 113, 116, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 85, 88, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 57, 60, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 29, 32, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 197, 200, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1.3);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(28, 28, 48, 0, 0, -24);
            this.states.body.collideWorldBounds = true;
        }
    }
}
