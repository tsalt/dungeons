///<reference path="../../../Lib/phaser/phaser.d.ts"/>
///<reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
///<reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class Minotaur extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 300, 300, 2.8, 96, 40);

            this.goldDropAmount = this.game.rnd.integerInRange(25, 100);

            this.dieSound = this.game.add.sound("minotaurDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            this.states = this.game.add.isoSprite(x, y, 0, "minotaur", 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 149, 156, ''), 10, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 125, 132, ''), 10, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 101, 108, ''), 10, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 77, 84, ''), 10, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 53, 60, ''), 10, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 29, 36, ''), 10, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 10, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 173, 180), 10, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 157, 160, ''), 5, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 133, 136, ''), 5, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 109, 112, ''), 5, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 85, 88, ''), 5, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 61, 64, ''), 5, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 37, 40, ''), 5, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 13, 16, ''), 5, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 181, 184, ''), 5, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 163, 168, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 139, 144, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 115, 120, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 91, 96, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 67, 72, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 44, 49, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 19, 24, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 187, 192, ''), 10, false);

            this.states.animations.add('castSpellS', Phaser.Animation.generateFrameNames('sprite', 161, 162, ''), 5, false);
            this.states.animations.add('castSpellSE', Phaser.Animation.generateFrameNames('sprite', 137, 138, ''), 5, false);
            this.states.animations.add('castSpellE', Phaser.Animation.generateFrameNames('sprite', 114, 115, ''), 5, false);
            this.states.animations.add('castSpellNE', Phaser.Animation.generateFrameNames('sprite', 89, 90, ''), 5, false);
            this.states.animations.add('castSpellN', Phaser.Animation.generateFrameNames('sprite', 65, 66, ''), 5, false);
            this.states.animations.add('castSpellNW', Phaser.Animation.generateFrameNames('sprite', 42, 43, ''), 5, false);
            this.states.animations.add('castSpellW', Phaser.Animation.generateFrameNames('sprite', 17, 18, ''), 5, false);
            this.states.animations.add('castSpellSW', Phaser.Animation.generateFrameNames('sprite', 185, 186, ''), 5, false);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 145, 148, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 121, 124, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 97, 100, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 73, 76, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 49, 52, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 25, 28, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 169, 172, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(2);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(32, 32, 48, 0, 0, -32);
            this.states.body.collideWorldBounds = true;
        }
    }
}
