/// <reference path="../../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../../UI/HealthBar.ts"/>
///<reference path="../../Spells/GoldDrop.ts"/>
///<reference path="../../Spells/DirectedAttacks/Arrow.ts"/>
///<reference path="../Enemy.ts"/>

module Dungeons {

    export class Archer extends Enemy {

        constructor(game: Phaser.Game, displayGroup: Phaser.Group, x: number, y: number) {
            super(game, displayGroup, 250, 250, 3.3, 42, 25);

            this.goldDropAmount = this.game.rnd.integerInRange(10, 80);

            this.dieSound = this.game.add.sound("skeletonKnightDie");
            this.dieSound.addMarker("die", 0, 1, 0.5, false);

            this.states = this.game.add.isoSprite(x, y, 0, "archer", 0, displayGroup);

            this.states.animations.add('runS', Phaser.Animation.generateFrameNames('sprite', 197, 204, ''), 10, true);
            this.states.animations.add('runSE', Phaser.Animation.generateFrameNames('sprite', 165, 172, ''), 10, true);
            this.states.animations.add('runE', Phaser.Animation.generateFrameNames('sprite', 133, 140, ''), 10, true);
            this.states.animations.add('runNE', Phaser.Animation.generateFrameNames('sprite', 101, 108, ''), 10, true);
            this.states.animations.add('runN', Phaser.Animation.generateFrameNames('sprite', 69, 76, ''), 10, true);
            this.states.animations.add('runNW', Phaser.Animation.generateFrameNames('sprite', 37, 44, ''), 10, true);
            this.states.animations.add('runW', Phaser.Animation.generateFrameNames('sprite', 5, 12, ''), 10, true);
            this.states.animations.add('runSW', Phaser.Animation.generateFrameNames('sprite', 229, 236, ''), 10, true);

            this.states.animations.add('attackS', Phaser.Animation.generateFrameNames('sprite', 205, 208, ''), 5, false);
            this.states.animations.add('attackSE', Phaser.Animation.generateFrameNames('sprite', 173, 176, ''), 5, false);
            this.states.animations.add('attackE', Phaser.Animation.generateFrameNames('sprite', 141, 144, ''), 5, false);
            this.states.animations.add('attackNE', Phaser.Animation.generateFrameNames('sprite', 109, 112, ''), 5, false);
            this.states.animations.add('attackN', Phaser.Animation.generateFrameNames('sprite', 77, 80, ''), 5, false);
            this.states.animations.add('attackNW', Phaser.Animation.generateFrameNames('sprite', 45, 48, ''), 5, false);
            this.states.animations.add('attackW', Phaser.Animation.generateFrameNames('sprite', 13, 16, ''), 5, false);
            this.states.animations.add('attackSW', Phaser.Animation.generateFrameNames('sprite', 237, 240, ''), 5, false);

            this.states.animations.add('dieS', Phaser.Animation.generateFrameNames('sprite', 211, 216, ''), 10, false);
            this.states.animations.add('dieSE', Phaser.Animation.generateFrameNames('sprite', 179, 184, ''), 10, false);
            this.states.animations.add('dieE', Phaser.Animation.generateFrameNames('sprite', 145, 152, ''), 10, false);
            this.states.animations.add('dieNE', Phaser.Animation.generateFrameNames('sprite', 113, 120, ''), 10, false);
            this.states.animations.add('dieN', Phaser.Animation.generateFrameNames('sprite', 81, 88, ''), 10, false);
            this.states.animations.add('dieNW', Phaser.Animation.generateFrameNames('sprite', 49, 56, ''), 10, false);
            this.states.animations.add('dieW', Phaser.Animation.generateFrameNames('sprite', 17, 24, ''), 10, false);
            this.states.animations.add('dieSW', Phaser.Animation.generateFrameNames('sprite', 243, 248, ''), 10, false);

            this.states.animations.add('castSpellS', Phaser.Animation.generateFrameNames('sprite', 217, 220, ''), 5, true);
            this.states.animations.add('castSpellSE', Phaser.Animation.generateFrameNames('sprite', 185, 188, ''), 5, true);
            this.states.animations.add('castSpellE', Phaser.Animation.generateFrameNames('sprite', 153, 156, ''), 5, true);
            this.states.animations.add('castSpellNE', Phaser.Animation.generateFrameNames('sprite', 121, 124, ''), 5, true);
            this.states.animations.add('castSpellN', Phaser.Animation.generateFrameNames('sprite', 89, 92, ''), 5, true);
            this.states.animations.add('castSpellNW', Phaser.Animation.generateFrameNames('sprite', 57, 60, ''), 5, true);
            this.states.animations.add('castSpellW', Phaser.Animation.generateFrameNames('sprite', 25, 28, ''), 5, true);
            this.states.animations.add('castSpellSW', Phaser.Animation.generateFrameNames('sprite', 249, 252, ''), 5, true);

            this.states.animations.add('fireArrowS', Phaser.Animation.generateFrameNames('sprite', 221, 224, ''), 5, false);
            this.states.animations.add('fireArrowSE', Phaser.Animation.generateFrameNames('sprite', 189, 192, ''), 5, false);
            this.states.animations.add('fireArrowE', Phaser.Animation.generateFrameNames('sprite', 157, 160, ''), 5, false);
            this.states.animations.add('fireArrowNE', Phaser.Animation.generateFrameNames('sprite', 125, 128, ''), 5, false);
            this.states.animations.add('fireArrowN', Phaser.Animation.generateFrameNames('sprite', 93, 96, ''), 5, false);
            this.states.animations.add('fireArrowNW', Phaser.Animation.generateFrameNames('sprite', 61, 64, ''), 5, false);
            this.states.animations.add('fireArrowW', Phaser.Animation.generateFrameNames('sprite', 29, 32, ''), 5, false);
            this.states.animations.add('fireArrowSW', Phaser.Animation.generateFrameNames('sprite', 253, 256, ''), 5, false);

            this.states.animations.add('idleS', Phaser.Animation.generateFrameNames('sprite', 193, 196, ''), 5, true);
            this.states.animations.add('idleSE', Phaser.Animation.generateFrameNames('sprite', 161, 164, ''), 5, true);
            this.states.animations.add('idleE', Phaser.Animation.generateFrameNames('sprite', 129, 132, ''), 5, true);
            this.states.animations.add('idleNE', Phaser.Animation.generateFrameNames('sprite', 97, 100, ''), 5, true);
            this.states.animations.add('idleN', Phaser.Animation.generateFrameNames('sprite', 65, 68, ''), 5, true);
            this.states.animations.add('idleNW', Phaser.Animation.generateFrameNames('sprite', 33, 36, ''), 5, true);
            this.states.animations.add('idleW', Phaser.Animation.generateFrameNames('sprite', 1, 4, ''), 5, true);
            this.states.animations.add('idleSW', Phaser.Animation.generateFrameNames('sprite', 225, 228, ''), 5, true);

            this.states.anchor.setTo(0.5);
            this.states.scale.setTo(1.3);

            this.states.animations.play(this.idleAnimation);

            this.game.physics.isoArcade.enable(this.states);
            this.states.body.setSize(28, 28, 48, 0, 0, -24);
            this.states.body.collideWorldBounds = true;
        }

        // fireArrow() {
        //     // if (this.checkUnitAction()) {
        //     this.currentState = UnitStates.CASTING;
        //     this.states.animations.play('fireArrow' + this.orientation).onComplete.addOnce(() => {
        //         let arrow = new Arrow(this.game, this.displayGroup, this.states.body.position.x,
        //             this.states.body.position.y, this.orientation, this.states.body.widthX);
        //         // this.spellCollection.push(arrow);
        //         this.currentState = UnitStates.IDLE;
        //     })
        // }
    }
}
