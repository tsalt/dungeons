/// <reference path="../../Lib/phaser/phaser.d.ts"/>
/// <reference path="../../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
/// <reference path="../UI/Joystick.ts"/>
/// <reference path="../Spells/Quake.ts"/>
/// <reference path="../Spells/Heal.ts"/>
///<reference path="../Spells/DirectedAttacks/Fireball.ts"/>
///<reference path="../Spells/DirectedAttacks/AttackHitbox.ts"/>
///<reference path="../UI/HeroHealthBar.ts"/>

module Dungeons {

    export enum UnitStates {IDLE, RUNNING, ATTACKING, BLOCKING, CASTING, DYING}

    export abstract class Unit {
        game: Phaser.Game;

        displayGroup;

        currentState;
        orientation: string = "SE";

        attackSize;
        attackHitbox;

        speed: number;
        idleAnimation: string;

        dieSound;

        constructor(game: Phaser.Game, displayGroup, attackSize: number, speed: number) {
            this.game = game;
            this.attackSize = attackSize;
            this.speed = speed;
            this.displayGroup = displayGroup;
        }

        checkUnitAction() {
            return (this.currentState != UnitStates.CASTING && this.currentState != UnitStates.ATTACKING
            && this.currentState != UnitStates.BLOCKING && this.currentState != UnitStates.DYING)
        }

        attack(states: Phaser.Sprite) {
            this.currentState = UnitStates.ATTACKING;
            this.attackHitbox = new AttackHitbox(this.game, states.body.position.x, states.body.position.y,
                this.orientation, this.attackSize, states.body.widthX);
            states.animations.play('attack' + this.orientation).onComplete.addOnce(() => {
                this.currentState = UnitStates.IDLE;
                this.attackHitbox.states.destroy();
                this.attackHitbox = null;
            });
        }

        dying(states: Phaser.Sprite) {
            this.currentState = UnitStates.DYING;
            this.dieSound.play("die");
            states.animations.play('die' + this.orientation);
            states.body.immovable = true;
            states.lifespan = 2500;
        }

        move(key: string, states: Phaser.Sprite) {
            this.currentState = UnitStates.RUNNING;
            states.body.immovable = false;
            states.animations.play('run' + key);
            this.orientation = key;
            this.idleAnimation = 'idle' + key;

            if (key === "NW") {
                states.body.position.x -= this.speed;
            }
            else if (key === "SE") {
                states.body.position.x += this.speed;
            }
            else if (key === "NE") {
                states.body.position.y -= this.speed;
            }
            else if (key === "SW") {
                states.body.position.y += this.speed;
            }
            else if (key === "S") {
                states.body.position.x += this.speed * 0.8;
                states.body.position.y += this.speed * 0.8;
            }
            else if (key === "W") {
                states.body.position.x -= this.speed * 0.6;
                states.body.position.y += this.speed * 0.6;
            }
            else if (key === "E") {
                states.body.position.x += this.speed * 0.6;
                states.body.position.y -= this.speed * 0.6;
            }
            else if (key === "N") {
                states.body.position.x -= this.speed * 0.8;
                states.body.position.y -= this.speed * 0.8;
            }
        }
    }
}