/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="Game.ts"/>
/// <reference path="../Classes/Units/Model.ts"/>

module Dungeons {
    export class ArenaStage extends Phaser.State {
        heroModel:Model;

        potionButton;

        potionBuySound;
        enterArmorySound;
        fireSound;

        currentGoldText;
        currentPotionsText;
        buyPotionText;
        priceOfPotionText;

        selectedHead:number;
        selectedArmor:number;
        selectedWeapon:number;
        selectedShield:number;

        init(selectedHead:number, selectedArmor:number, selectedWeapon:number, selectedShield:number) {
            this.selectedHead = selectedHead || 0;
            this.selectedArmor = selectedArmor || 0;
            this.selectedWeapon = selectedWeapon || 0;
            this.selectedShield = selectedShield || 0;
        }

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainBackground');
        }

        create() {
            if (!localStorage.getItem("gold")) {
                localStorage.setItem("gold", "200");
                localStorage.setItem("potions", "0");
                localStorage.setItem("collectedGold", "0");
                localStorage.setItem("killedMonsters", "0");
            }

            let backgroundPart = this.game.add.image(this.game.width * 0.35, this.game.height * 0.45, 'customizationPart');
            backgroundPart.anchor.set(0.5);

            let shopPart = this.game.add.image(this.game.width * 0.83, this.game.height * 0.41, 'shopPart');
            shopPart.anchor.set(0.5);
            shopPart.scale.set(1);

            let fire = this.game.add.sprite(960, 470, "fire");
            fire.animations.add('fire', Phaser.Animation.generateFrameNames('', 40, 80), 25, true);
            fire.animations.play("fire");
            fire.anchor.set(0.5);
            fire.scale.set(1.1);

            this.fireSound = this.game.add.audio('fire');
            this.fireSound.addMarker('fireplace', 0, 3, 0.5, true);
            this.fireSound.play("fireplace");

            this.potionBuySound = this.game.add.audio('coinDrop');
            this.potionBuySound.addMarker('coinDrop', 0, 1, 1);

            this.enterArmorySound = this.game.add.audio('armory');
            this.enterArmorySound.addMarker('armory', 0, 1, 0.5);

            let torch = this.game.add.image(900, 473, "LampStand");

            // Title
            let titleStyle = {font: '70pt scochflashfont', fill: '#B6352D', align: 'center'};
            let title = this.game.add.text(500, 80, 'Lobby', titleStyle);
            title.anchor.set(0.5);

            let armoryButton = this.game.add.button(this.game.width * 0.22, this.game.height * 0.6, 'button1', customization, this, 1, 2, 0);
            armoryButton.scale.set(0.5);
            armoryButton.anchor.setTo(0.5);

            let style = {font: '25px scochflashfont', fill: 'white'};
            let armoryText = this.game.add.text(this.game.width * 0.22, this.game.height * 0.6, "Armory", style);
            armoryText.stroke = 'black';
            armoryText.strokeThickness = 3;
            armoryText.anchor.set(0.5);

            let fightButton = this.game.add.button(this.game.width * 0.5, this.game.height * 0.9, 'button1', play, this, 1, 2, 0);
            fightButton.scale.set(0.7);
            fightButton.anchor.setTo(0.5);

            let fightStyle = {font: '28pt scochflashfont', fill: 'white'};
            let fightText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.9, "Fight", fightStyle);
            fightText.stroke = "rgba(255,25,25,0.6)";
            fightText.strokeThickness = 3;
            fightText.anchor.set(0.5);

            // Gold
            let goldIcon = this.game.add.image(this.game.width * 0.72, this.game.height * 0.29, 'goldIcon');
            goldIcon.anchor.set(0.5);
            goldIcon.scale.set(-0.7, 0.6);

            this.currentGoldText = this.game.add.text(this.game.width * 0.76, this.game.height * 0.31, localStorage.getItem("gold"), style);
            this.currentGoldText.anchor.set(0.5);

            // Potions
            let potionIcon = this.game.add.image(this.game.width * 0.82, this.game.height * 0.41, 'redPotion');
            potionIcon.anchor.set(0.5);
            potionIcon.scale.set(1.2);

            let potionStyle = {font: '43px scochflashfont', fill: 'white'};
            this.currentPotionsText = this.game.add.text(this.game.width * 0.85, this.game.height * 0.43, localStorage.getItem("potions"), potionStyle);
            this.currentPotionsText.anchor.set(0.5);
            this.currentPotionsText.stroke = 'black';
            this.currentPotionsText.strokeThickness = 4;

            this.potionButton = this.game.add.button(this.game.width * 0.83, this.game.height * 0.6, 'button1', buyPotion, this, 1, 2, 0);
            this.potionButton.scale.set(0.5);
            this.potionButton.anchor.setTo(0.5);

            this.buyPotionText = this.game.add.text(this.game.width * 0.83, this.game.height * 0.6, "Buy", style);
            this.buyPotionText.anchor.set(0.5);

            this.priceOfPotionText = this.game.add.text(this.game.width * 0.83, this.game.height * 0.53, "Cost: " + (( parseInt(localStorage.getItem("potions")) + 1) * 100), style);
            this.priceOfPotionText.anchor.set(0.5);

            let highscoreText = this.game.add.text(this.game.width * 0.53, this.game.height * 0.3, "Highscore", style);
            highscoreText.anchor.set(0.5);

            let monstersKilledText = this.game.add.text(this.game.width * 0.42, this.game.height * 0.37, "Most monsters killed:", style);
            let mostersKilled = this.game.add.text(this.game.width * 0.58, this.game.height * 0.45, localStorage.getItem("killedMonsters"), style);
            mostersKilled.anchor.set(1, 0.5);

            let monstersKilledIcon = this.game.add.image(this.game.width * 0.6, this.game.height * 0.445, 'monstersKilled');
            monstersKilledIcon.anchor.set(0.5);
            monstersKilledIcon.scale.set(0.8);

            let goldCollectedText = this.game.add.text(this.game.width * 0.42, this.game.height * 0.5, "Most gold collected:", style);
            let goldCollected = this.game.add.text(this.game.width * 0.58, this.game.height * 0.58, localStorage.getItem("collectedGold"), style);
            goldCollected.anchor.set(1, 0.5);

            let goldPiecesIcon = this.game.add.image(this.game.width * 0.6, this.game.height * 0.58, 'goldPieces');
            goldPiecesIcon.anchor.set(0.5);
            goldPiecesIcon.scale.set(0.6);

            this.DrawNewModel();
        }

        update() {
            if (parseInt(localStorage.getItem("potions")) == 5 || parseInt(localStorage.getItem("gold")) < (parseInt(localStorage.getItem("potions")) + 1) * 100) {
                this.potionButton.tint = 0x70737A;
                this.buyPotionText.tint = 0x70737A;
                this.potionButton.freezeFrames = true;
                this.potionButton.inputEnabled = false;
            }

            if (parseInt(localStorage.getItem("potions")) < 5) {
                this.priceOfPotionText.text = "Cost: " + (( parseInt(localStorage.getItem("potions")) + 1) * 100);
            }
            else {
                this.priceOfPotionText.text = "Limit reached";
            }
            this.currentGoldText.text = localStorage.getItem("gold");
            this.currentPotionsText.text = localStorage.getItem("potions");
        }

        DrawNewModel() {
            this.heroModel = new Model(this.game, this.game.width * 0.22, this.game.height * 0.4,
                this.selectedHead,
                this.selectedArmor,
                this.selectedWeapon,
                this.selectedShield);
        }
    }
    function customization() {
        this.fireSound.stop();
        this.enterArmorySound.play("armory");
        this.game.state.start("Customization");
    }

    function play() {
        this.fireSound.stop();
        this.game.state.start("Game", true, false,
            this.heroModel.head[this.selectedHead],
            this.heroModel.armor[this.selectedArmor],
            this.heroModel.weapon[this.selectedWeapon],
            this.heroModel.shield[this.selectedShield]);
    }

    function buyPotion() {
        if (parseInt(localStorage.getItem("potions")) != 5 && parseInt(localStorage.getItem("gold")) > (parseInt(localStorage.getItem("potions")) + 1) * 100) {
            this.potionBuySound.play("coinDrop");
            localStorage.setItem("gold", (parseInt(localStorage.getItem("gold")) - (parseInt(localStorage.getItem("potions")) + 1) * 100).toString());
            localStorage.setItem("potions", (parseInt(localStorage.getItem("potions")) + 1).toString());
        }
    }
}
