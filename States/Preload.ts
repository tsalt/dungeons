/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="MainMenu.ts"/>

module Dungeons {
    export class Preload extends Phaser.State {
        loadingBarFrame;
        loadingBarFill;
        progressText;
        mask;

        preDrawnElements = [];

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainBackground');

            let bloodPart = this.game.add.image(this.game.width * 0.45, this.game.height * 0.5, 'bloodPart');
            bloodPart.anchor.set(0.5);
            bloodPart.alpha = 0.5;

            let teamLogo = this.game.add.image(this.game.width * 0.94, this.game.height * 0.88, "teamLogo");
            teamLogo.scale.set(0.6);
            teamLogo.anchor.set(0.5, 0.5);

            this.loadingBarFrame = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "loadingBarFrame");
            this.loadingBarFrame.anchor.set(0.5);

            this.loadingBarFill = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "loadingBarFill");
            this.loadingBarFill.anchor.set(0.5);

            this.mask = this.game.add.graphics(0, 0);
            this.mask.lineStyle(55, 0xffffff);
            this.loadingBarFill.mask = this.mask;

            let style = {font: "40px Arial", fill: "white"};
            this.progressText = this.game.add.text(this.game.width * 0.5 + 3, this.game.height * 0.5 + 18, "0%", style);
            this.progressText.anchor.set(0.5);

            this.game.load.onFileComplete.add(this.fileComplete, this);

            // this.game.load.atlasJSONHash('spritesheet', 'assets/spritesheet.png', 'assets/spritesheet.json');

            //HP bars
            this.game.load.image('emptyHpBar', "assets/GUI/HealthBars/emptyHpBar.png");
            this.game.load.image('fullHpBar', "assets/GUI/HealthBars/fullHpBar.png");
            this.game.load.image('emptyFloatingBar', "assets/GUI/HealthBars/emptyFloatingBar.png");
            this.game.load.image('fullFloatingBar', "assets/GUI/HealthBars/fullFloatingBar.png");
            this.game.load.image('manaBar', "assets/GUI/HealthBars/manaBar.png");

            //Spells
            this.game.load.spritesheet('heal', 'assets/GUI/Spells/heal.png', 64, 64);
            this.game.load.spritesheet('fireball', 'assets/GUI/Spells/fireball.png', 64, 64);
            this.game.load.atlasJSONHash('quake', 'assets/GUI/Spells/quake.png', 'assets/GUI/Spells/quake.json');
            this.game.load.atlasJSONHash('explosion', 'assets/GUI/Spells/explosion.png', 'assets/GUI/Spells/explosion.json');
            this.game.load.atlasJSONHash('projectiles', 'assets/GUI/Spells/projectiles.png', 'assets/GUI/Spells/projectiles.json');

            //Blood
            this.game.load.spritesheet('blood1', 'assets/GUI/blood/blood_hit_01.png', 128, 128);
            this.game.load.spritesheet('blood2', 'assets/GUI/blood/blood_hit_02.png', 128, 128);
            this.game.load.spritesheet('blood3', 'assets/GUI/blood/blood_hit_03.png', 128, 128);
            this.game.load.spritesheet('blood4', 'assets/GUI/blood/blood_hit_04.png', 128, 128);

            // objects
            this.game.load.spritesheet('gold5', 'assets/GUI/gold5.png', 64, 128);
            this.game.load.spritesheet('gold25', 'assets/GUI/gold25.png', 64, 128);
            this.game.load.spritesheet('gold50', 'assets/GUI/gold50.png', 64, 128);

            // Characters
            this.game.load.atlasJSONHash('archer', 'assets/Characters/archer.png', 'assets/Characters/archer.json');
            this.game.load.atlasJSONHash('mudGolem', 'assets/Characters/mudGolem.png', 'assets/Characters/mudGolem.json');
            this.game.load.atlasJSONHash('werebear', 'assets/Characters/werebear.png', 'assets/Characters/werebear.json');
            this.game.load.atlasJSONHash('minotaur', 'assets/Characters/minotaur.png', 'assets/Characters/minotaur.json');
            this.game.load.atlasJSONHash('skeletonKnight', 'assets/Characters/skeletonKnight.png', 'assets/Characters/skeletonKnight.json');
            this.game.load.atlasJSONHash('orc', 'assets/Characters/orc.png', 'assets/Characters/orc.json');
            this.game.load.atlasJSONHash('zombie', 'assets/Characters/zombie.png', 'assets/Characters/zombie.json');
            this.game.load.atlasJSONHash('fireAnt', 'assets/Characters/fireAnt.png', 'assets/Characters/fireAnt.json');
            this.game.load.atlasJSONHash('iceAnt', 'assets/Characters/iceAnt.png', 'assets/Characters/iceAnt.json');
            this.game.load.atlasJSONHash('blackAnt', 'assets/Characters/blackAnt.png', 'assets/Characters/blackAnt.json');

            //Hero customization
            this.game.load.spritesheet('Plate', 'assets/Characters/Hero/steel_armor.png', 128, 128);
            this.game.load.spritesheet('Cloth', 'assets/Characters/Hero/clothes.png', 128, 128);
            this.game.load.spritesheet('Leather', 'assets/Characters/Hero/leather_armor.png', 128, 128);
            this.game.load.spritesheet('Shield', 'assets/Characters/Hero/shield.png', 128, 128);
            this.game.load.spritesheet('Hood', 'assets/Characters/Hero/male_head3.png', 128, 128);
            this.game.load.spritesheet('Bald', 'assets/Characters/Hero/male_head2.png', 128, 128);
            this.game.load.spritesheet('Short Hair', 'assets/Characters/Hero/male_head1.png', 128, 128);
            this.game.load.spritesheet('Buckler', 'assets/Characters/Hero/buckler.png', 128, 128);
            this.game.load.spritesheet('Dagger', 'assets/Characters/Hero/dagger.png', 128, 128);
            this.game.load.spritesheet('Great Staff', 'assets/Characters/Hero/greatstaff.png', 128, 128);
            this.game.load.spritesheet('Great Sword', 'assets/Characters/Hero/greatsword.png', 128, 128);
            this.game.load.spritesheet('Long Sword', 'assets/Characters/Hero/longsword.png', 128, 128);
            this.game.load.spritesheet('Rod', 'assets/Characters/Hero/rod.png', 128, 128);
            this.game.load.spritesheet('Short Sword', 'assets/Characters/Hero/shortsword.png', 128, 128);
            this.game.load.spritesheet('Staff', 'assets/Characters/Hero/staff.png', 128, 128);
            this.game.load.spritesheet('None', 'assets/Characters/Hero/empty.png', 128, 128);

            // Buttons and Icons
            this.game.load.spritesheet('button1', 'assets/Buttons/button1.png', 362, 147);
            this.game.load.spritesheet('button2', 'assets/Buttons/button2.png', 258, 80);
            this.game.load.image('flameOrb', 'assets/Buttons/flameOrb.png');
            this.game.load.image('attackOrb', 'assets/Buttons/attackOrb.png');
            this.game.load.image('bombOrb', 'assets/Buttons/bombOrb.png');
            this.game.load.image('buttonBackground', 'assets/Buttons/buttonBackground.png');
            this.game.load.image('menuButton', 'assets/Buttons/menuButton.png');
            this.game.load.image('arrowButton', "assets/GUI/ButtonArrow.png");
            this.game.load.image('goldIcon', "assets/GUI/goldIcon.png");
            this.game.load.image('redPotion', "assets/GUI/redPotion.png");
            this.game.load.image('goldPieces', "assets/GUI/goldPieces.png");
            this.game.load.image('monstersKilled', "assets/GUI/monstersKilled.png");
            this.game.load.image('star', "assets/GUI/star.png");

            // Sounds
            this.game.load.audio('soundOnHover', 'assets/Sounds/interface1.ogg');
            this.game.load.audio('soundOnPress', 'assets/Sounds/interface2.ogg');
            this.game.load.audio('coinDrop', 'assets/Sounds/coins-shake-01.ogg');
            this.game.load.audio('fire', 'assets/Sounds/fireplace.ogg');
            this.game.load.audio('heroAttack', 'assets/Sounds/swing3.ogg');
            this.game.load.audio('fireball', 'assets/Sounds/fireball.ogg');
            this.game.load.audio('quake', 'assets/Sounds/caveIn.ogg');
            this.game.load.audio('drinkPotion', 'assets/Sounds/bubble.ogg');
            this.game.load.audio('armory', 'assets/Sounds/chainmail2.ogg');
            this.game.load.audio('fireballCollide', 'assets/Sounds/explosion.ogg');
            this.game.load.audio('skeletonKnightDie', 'assets/Sounds/Monster/monster-1.ogg');
            this.game.load.audio('minotaurDie', 'assets/Sounds/Monster/monster-2.ogg');
            this.game.load.audio('mudGolemDie', 'assets/Sounds/Monster/monster-3.ogg');
            this.game.load.audio('werebearDie', 'assets/Sounds/Monster/monster-4.ogg');
            this.game.load.audio('zombieDie', 'assets/Sounds/Monster/monster-5.ogg');
            this.game.load.audio('orcDie', 'assets/Sounds/Monster/monster-7.ogg');
            this.game.load.audio('antDie', 'assets/Sounds/Monster/monster-6.ogg');
            this.game.load.audio('playerDeath', 'assets/Sounds/playerDeath.ogg');
            this.game.load.audio('mainMenuSound', 'assets/Sounds/mainMenuSound.ogg');

            //Background
            this.game.load.image('popupWindow', "assets/GUI/popupMenuBackground.png");
            this.game.load.image('blackScreen', "assets/GUI/blackScreen.jpg");
            this.game.load.image('buttonPlatform', "assets/GUI/buttonBackground.png");
            this.game.load.image('mainMenuPart', "assets/Graphics/mainMenuPart.png");
            this.game.load.image('shopPart', "assets/Graphics/shopPart.png");
            this.game.load.image('sword', "assets/Graphics/sword.png");
            this.game.load.image('shield', "assets/Graphics/shield.png");
            this.game.load.image('helmet', "assets/Graphics/helmet.png");
            this.game.load.image('customizationPart', "assets/GUI/customizationPart.png");
            this.game.load.image('itemPlatform', "assets/GUI/itemPlatform.png");
            this.game.load.image('torchSide', "assets/GUI/torchSide.png");
            this.game.load.image('LampStand', "assets/GUI/LampStand.png");
            this.game.load.atlasJSONHash('fire', "assets/GUI/fire.png", 'assets/GUI/fire.json');
            this.game.load.spritesheet('spawn', "assets/GUI/spawn.png", 64, 64);

            // Map stuff
            this.game.load.image("bg", "assets/map_floors_configs/bg.png");
            this.game.load.spritesheet("set1", "assets/Tiles/sprites.png", 256, 256, 11);
            this.game.load.text("level1_1", "assets/map_floors_configs/level1_1.csv", true);

            //Joystick
            this.game.load.image('joystick_base', 'assets/Joystick/compass.png');
            this.game.load.image("joystick_segment", "assets/Joystick/touch_segment.png");
            this.game.load.image("joystick_knob", "assets/Joystick/touch.png");
        }

        create() {
            this.game.time.events.add(0, () => {
                this.game.state.start("MainMenu");
            }, this);
        }

        fileComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
            this.progressText.setText(progress + "%");
            this.mask.arc(this.game.width * 0.5 - 2, this.game.height * 0.5 + 15, 100, this.game.math.degToRad(90), this.game.math.degToRad(progress * 3.6 + 91), false);
        }
    }
}