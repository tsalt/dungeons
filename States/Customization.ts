/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="Game.ts"/>
/// <reference path="../Classes/Units/Model.ts"/>

module Dungeons {
    export class Customization extends Phaser.State {
        heroModel: Model;

        readyButton;
        readyText;

        title;
        armorText;
        headText;
        weaponText;
        shieldText;

        selectedState: number;

        selectedHead: number = 0;
        selectedArmor: number = 0;
        selectedWeapon: number = 0;
        selectedShield: number = 0;

        textGroup;

        preDrawnElements = [];

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainBackground');
        }

        create() {
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Plate"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Cloth"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Leather"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Shield"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Hood"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Bald"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Short Hair"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Buckler"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Dagger"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Great Staff"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Great Sword"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Long Sword"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Rod"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Short Sword"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "Staff"));
            this.preDrawnElements.push(this.game.add.isoSprite(0, 0, 0, "None"));
            this.preDrawnElements.forEach(function(element){
                element.kill();
            },this);
            this.preDrawnElements.splice(0, this.preDrawnElements.length);

            this.textGroup = this.game.add.group();

            this.selectedState = 0;

            let sword = this.game.add.image(this.game.width * 0.6, this.game.height * 0.15, 'sword');
            sword.anchor.set(0.5);

            let shield = this.game.add.image(this.game.width * 0.69, this.game.height * 0.5, 'shield');
            shield.anchor.set(0.7);

            let helmet = this.game.add.image(this.game.width * 0.15, this.game.height * 0.22, 'helmet');
            helmet.anchor.set(0.5);
            helmet.scale.set(0.86);
            helmet.angle = -8;

            let customizationPart = this.game.add.image(this.game.width * 0.35, this.game.height * 0.45, 'customizationPart');
            customizationPart.anchor.set(0.5);

            this.DrawNewModel();

            // Title
            let titleStyle = {font: '70pt scochflashfont', fill: '#B6352D', align: 'center'};
            this.title = this.game.add.text(500, 80, 'Armory', titleStyle);
            this.title.anchor.set(0.5);
            this.textGroup.add(this.title);

            //Buttons
            let modelBackButton = this.game.add.button(this.game.width * 0.22 - 180, this.game.height * 0.46, 'arrowButton', this.previousState, this, 1, 2, 0);
            modelBackButton.scale.set(-0.7);
            modelBackButton.anchor.setTo(0.5);

            let modelNextButton = this.game.add.button(this.game.width * 0.22 + 170, this.game.height * 0.46, 'arrowButton', this.nextState, this, 1, 2, 0);
            modelNextButton.scale.set(0.7);
            modelNextButton.anchor.setTo(0.5);

            this.readyButton = this.game.add.button(this.game.width * 0.85, this.game.height * 0.9, 'button1', toArenaStage, this, 1, 2, 0);
            this.readyButton.scale.set(0.7);
            this.readyButton.anchor.setTo(0.5);

            let readyStyle = {font: '28pt scochflashfont', fill: 'white'};
            this.readyText = this.game.add.text(this.game.width * 0.85, this.game.height * 0.9, "Ready", readyStyle);
            this.readyText.stroke = 'black';
            this.readyText.strokeThickness = 3;
            this.readyText.anchor.set(0.5);
            this.textGroup.add(this.readyText);

            // Item text
            let style = {font: "18px scochflashfont", fill: "#FF3100"};
            this.headText = this.game.add.text(1080, 190, this.heroModel.head[this.selectedHead], style);
            this.headText.anchor.set(0.5);
            this.headText.stroke = "rgba(255,200,200,0.5)";
            this.headText.strokeThickness = 0.5;
            this.textGroup.add(this.headText);

            this.armorText = this.game.add.text(1080, 290, this.heroModel.armor[this.selectedArmor], style);
            this.armorText.anchor.set(0.5);
            this.textGroup.add(this.armorText);

            this.weaponText = this.game.add.text(1080, 390, this.heroModel.weapon[this.selectedWeapon], style);
            this.weaponText.anchor.set(0.5);
            this.textGroup.add(this.weaponText);

            this.shieldText = this.game.add.text(1080, 490, this.heroModel.shield[this.selectedShield], style);
            this.shieldText.anchor.set(0.5);
            this.textGroup.add(this.shieldText);

            this.createField(1080, 180, "Head");
            this.createField(1080, 280, "Armor");
            this.createField(1080, 380, "Weapon");
            this.createField(1080, 480, "Shield");

            this.game.world.bringToTop(this.textGroup);
        }

        checkTypeOfEquipment(typeOfEquipment, directionOfChange) {
            if (directionOfChange == "Next") {
                if (typeOfEquipment == "Head") {
                    this.selectedHead = this.nextItem(this.heroModel.head, this.selectedHead, this.headText, 2);
                }
                else if (typeOfEquipment == "Armor") {
                    this.selectedArmor = this.nextItem(this.heroModel.armor, this.selectedArmor, this.armorText, 2);
                }
                else if (typeOfEquipment == "Weapon") {
                    this.selectedWeapon = this.nextItem(this.heroModel.weapon, this.selectedWeapon, this.weaponText, 6);
                }
                else if (typeOfEquipment == "Shield") {
                    this.selectedShield = this.nextItem(this.heroModel.shield, this.selectedShield, this.shieldText, 2);
                }
            }
            else {
                if (typeOfEquipment == "Head") {
                    this.selectedHead = this.previousItem(this.heroModel.head, this.selectedHead, this.headText, 2);
                }
                else if (typeOfEquipment == "Armor") {
                    this.selectedArmor = this.previousItem(this.heroModel.armor, this.selectedArmor, this.armorText, 2);
                }
                else if (typeOfEquipment == "Weapon") {
                    this.selectedWeapon = this.previousItem(this.heroModel.weapon, this.selectedWeapon, this.weaponText, 6);
                }
                else if (typeOfEquipment == "Shield") {
                    this.selectedShield = this.previousItem(this.heroModel.shield, this.selectedShield, this.shieldText, 2);
                }
            }
        }

        createField(positionX: number, positionY: number, textToDisplay: string) {
            let buttonLeft = this.game.add.button(positionX - 128, positionY, 'arrowButton', function () {
                this.checkTypeOfEquipment(textToDisplay, "Next");
                this.DrawNewModel();
            }, this, 1, 2, 0);
            buttonLeft.scale.set(-0.7);
            buttonLeft.anchor.setTo(0.5);
            let buttonRight = this.game.add.button(positionX + 128, positionY, 'arrowButton', function () {
                this.checkTypeOfEquipment(textToDisplay, "Previous");
                this.DrawNewModel();
            }, this, 1, 2, 0);
            buttonRight.scale.set(0.7);
            buttonRight.anchor.setTo(0.5);

            let itemPlatform = this.game.add.image(positionX, positionY, 'itemPlatform');
            itemPlatform.anchor.set(0.5);

            let style = {font: "13px scochflashfont", fill: "#FF310A"};
            let text = this.game.add.text(positionX, positionY - 16, textToDisplay, style);
            text.anchor.set(0.5);
            this.textGroup.add(text);
        }

        previousItem(itemToChange, selectedItem, textToChange, rangeOfSelection) {
            this.ClearOldModel();

            if (selectedItem > 0) {
                selectedItem--;
            }
            else {
                selectedItem = rangeOfSelection;
            }
            textToChange.text = itemToChange[selectedItem];
            return selectedItem;
        }

        nextItem(itemToChange, selectedItem, textToChange, rangeOfSelection) {
            this.ClearOldModel();

            if (selectedItem < rangeOfSelection) {
                selectedItem++;
            }
            else {
                selectedItem = 0;
            }
            textToChange.text = itemToChange[selectedItem];
            return selectedItem;
        }

        previousState() {
            if (this.selectedState > 0) {
                this.selectedState--;
            }
            else {
                this.selectedState = 7;
            }

            for (let i = 0; i < 4; i++) {
                this.heroModel.states[i].animations.play(this.heroModel.idleAnimations[this.selectedState]);
            }
        }

        nextState() {
            if (this.selectedState < 7) {
                this.selectedState++;
            }
            else {
                this.selectedState = 0;
            }
            for (let i = 0; i < 4; i++) {
                this.heroModel.states[i].animations.play(this.heroModel.idleAnimations[this.selectedState]);
            }
        }

        ClearOldModel() {
            for (let i = 0; i < 4; i++) {
                this.heroModel.states[i].destroy();
            }
        }

        DrawNewModel() {
            this.heroModel = new Model(this.game, this.game.width * 0.22, this.game.height * 0.4,
                this.selectedHead,
                this.selectedArmor,
                this.selectedWeapon,
                this.selectedShield);

            for (let i = 0; i < 4; i++) {
                this.heroModel.states[i].animations.play(this.heroModel.idleAnimations[this.selectedState]);
            }
        }
    }

    function toArenaStage() {
        this.game.state.start("ArenaStage", true, false,
            this.selectedHead,
            this.selectedArmor,
            this.selectedWeapon,
            this.selectedShield);
    }
}