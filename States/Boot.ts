///<reference path="../Lib/phaser/phaser.d.ts"/>
///<reference path="Preload.ts"/>

module Dungeons {
    export class Boot extends Phaser.State {

        preload() {
            this.game.load.image('mainBackground', "assets/Graphics/mainBackground.png");
            this.game.load.image('bloodPart', "assets/Graphics/bloodPart.png");
            this.game.load.image("zaribaLogo", "assets/Graphics/zaribaLogo.png");
            this.game.load.image("teamLogo", "assets/Graphics/SaltLogo.png");

            // For preloader
            this.game.load.image("loadingBarFrame", "assets/Graphics/loadingBar_frame.png");
            this.game.load.image("loadingBarFill", "assets/Graphics/loadingBar_fill.png");
            this.game.add.plugin(Phaser.Plugin.Debug);
        }

        create() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainBackground');

            let bloodPart = this.game.add.image(this.game.width * 0.45, this.game.height * 0.5, 'bloodPart');
            bloodPart.anchor.set(0.5);
            bloodPart.alpha = 0.5;

            this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
            this.game.time.advancedTiming = true;

            let bootLogo1 = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "zaribaLogo");
            bootLogo1.anchor.set(0.5, 0.5);
            bootLogo1.alpha = 0;


            // TODO: Change BEFORE RELEASE
            let tween = this.game.add.tween(bootLogo1).to({alpha: 1}, 10, "Linear", true, 250, 0);
            let tween2 = this.game.add.tween(bootLogo1).to({alpha: 0}, 10, "Linear", false, 10, 0);
            // let tween = this.game.add.tween(bootLogo1).to({alpha: 1}, 2500, "Linear", true, 250, 0);
            // let tween2 = this.game.add.tween(bootLogo1).to({alpha: 0}, 1000, "Linear", false, 1000, 0);

            let teamLogo = this.game.add.image(this.game.width * 0.94, this.game.height * 0.88, "teamLogo");
            teamLogo.scale.set(0.6);
            teamLogo.anchor.set(0.5, 0.5);

            tween.onComplete.add(()=> {
                tween2.start();
            });
            tween2.onComplete.add(()=> {
                this.game.state.start("Preload");
            });
        }
    }
}