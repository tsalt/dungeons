/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="MainMenu.ts"/>

module Dungeons {
    export class GameOver extends Phaser.State {
        collectedGold:number;
        killedMonsters:number;

        init(collectedGold:number, killedMonsters:number) {
            this.collectedGold = collectedGold;
            this.killedMonsters = killedMonsters;
        }

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainBackground');

            let bloodPart = this.game.add.image(this.game.width * 0.45, this.game.height * 0.5, 'bloodPart');
            bloodPart.anchor.set(0.5);
            bloodPart.alpha = 0.5;

            this.addTitle(100, "You Died");

            //Monsters
            let style = {font: '45pt scochflashfont', fill: '#792509'};
            let monsterText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.35, "Monsters killed: " + this.killedMonsters, style);
            monsterText.stroke = "rgba(255,255,255)";
            monsterText.strokeThickness = 2;
            monsterText.anchor.set(0.5);
            if ((this.killedMonsters > parseInt(localStorage.getItem("killedMonsters"))) || localStorage.getItem("killedMonsters") === null) {
                localStorage.setItem("killedMonsters", this.killedMonsters.toString());
                this.newRecord(this.game.height * 0.45);
            }

            //Gold
            let goldText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.6, "Gold collected: " + this.collectedGold, style);
            goldText.anchor.set(0.5);
            if ((this.collectedGold > parseInt(localStorage.getItem("collectedGold"))) || localStorage.getItem("collectedGold") === null) {
                localStorage.setItem("collectedGold", this.collectedGold.toString());
                this.newRecord(this.game.height * 0.7);
            }

            // Click to Exit
            let fadedBackground = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "blackScreen");
            fadedBackground.anchor.setTo(0.5);
            fadedBackground.alpha = 0;
            fadedBackground.inputEnabled = true;
            fadedBackground.events.onInputDown.add(this.toArenaStage, this);
        }

        addTitle(height, text) {
            let optionStyle = {font: '80pt scochflashfont', fill: '#B6352D'};
            let txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.anchor.set(0.5);
        }

        newRecord(height) {
            let optionStyle = {font: '30px scochflashfont', fill: '#D0A82D'};
            let image = this.game.add.image(this.game.width * 0.5, height - 5, 'star');
            image.anchor.set(0.5);
            image.scale.set(0.5);
            image.alpha = 0.7;
            let txt = this.game.add.text(this.game.width * 0.5, height, "New Record!", optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }


        toArenaStage() {
            this.game.state.start("ArenaStage", true, false);
        }
    }
}