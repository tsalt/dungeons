/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="Game.ts"/>
/// <reference path="Customization.ts"/>

module Dungeons {
    export class MainMenu extends Phaser.State {
        soundOnHover;
        soundOnPress;
        states:Phaser.Sprite;
        backgroundSound;
        fireSound;

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainBackground');
        }

        create() {
            let fireLeft = this.game.add.sprite(218, 300, "fire");
            fireLeft.animations.add('fire', Phaser.Animation.generateFrameNames('', 40, 80), 25, true);
            fireLeft.animations.play("fire");

            let fireRight = this.game.add.sprite(970, 300, "fire");
            fireRight.animations.add('fire', Phaser.Animation.generateFrameNames('', 40, 80), 25, true);
            fireRight.animations.play("fire");

            let torchLeft = this.game.add.image(320, 370, "torchSide");
            torchLeft.scale.set(-1, 1);

            let torchRight = this.game.add.image(960, 370, "torchSide");

            let menuPart = this.game.add.image(this.game.width * 0.5, this.game.height * 0.6, 'mainMenuPart');
            menuPart.anchor.set(0.5);

            this.soundOnHover = this.game.add.audio('soundOnHover');
            this.soundOnPress = this.game.add.audio('soundOnPress');

            this.backgroundSound = this.game.add.audio('mainMenuSound');
            this.backgroundSound.addMarker('fireplace', 0, 40, 0.5, true);
            this.backgroundSound.play("fireplace");

            this.fireSound = this.game.add.audio('fire');
            this.fireSound.addMarker('fireplace', 0, 3, 0.5, true);
            this.fireSound.play("fireplace");

            this.addTitle(100, 'Survival');

            this.addMenuOption(315, 'Play', this.play, "#E6A12E", 5);
            this.addMenuOption(435, 'Store', this.store, "white", 3);
            this.addMenuOption(555, 'Credits', this.credits, "white", 3);
        }

        addTitle(height, text) {
            let optionStyle = {font: '80pt scochflashfont', fill: '#B6352D'};
            let txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.anchor.set(0.5);
        }

        addMenuOption(height, text, callback, fill, stroke) {
            let optionStyle = {font: '30pt scochflashfont', fill: fill};
            let txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.anchor.set(0.5);

            txt.stroke = "rgba(255,25,25,0.6)";
            txt.strokeThickness = stroke;
            txt.inputEnabled = true;
            txt.events.onInputDown.add(callback, this);
        }

        play() {
            this.fireSound.stop();
            this.backgroundSound.stop();
            this.game.state.start("ArenaStage");
        }

        store() {
        }

        credits() {
        }
    }

}