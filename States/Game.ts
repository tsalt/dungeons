///<reference path="../Lib/isometric-plugin/phaser.plugin.isometric.d.ts"/>
///<reference path="../Lib/phaser/phaser.d.ts"/>
///<reference path="../Classes/UI/UIManager.ts"/>
///<reference path="../Classes/Units/Hero.ts"/>
///<reference path="../Classes/MapManager/MapFloorConfig.ts"/>
///<reference path="../Classes/MapManager/MapManager.ts"/>
///<reference path="../Classes/GameControllers/EnemySpawner.ts"/>
///<reference path="../Classes/GameControllers/Renderer.ts"/>
///<reference path="../Classes/GameControllers/CollisionDetector.ts"/>
///<reference path="../Classes/GameControllers/PathFinding.ts"/>
///<reference path="GameOver.ts"/>

/**
 * NOTE: According to the compiler there is/are error/s in this file. Ignore them! Everything is OK. This is because the
 * compiler doesn't know about the Isometric plugin and its types.
 */
module Dungeons {
    export class Game extends Phaser.State {
        mapManager: MapManager;
        userInterface: UIManager;

        hero: Hero;

        selectedHead: string;
        selectedArmor: string;
        selectedWeapon: string;
        selectedShield: string;

        enemies = [];
        pickUpItems = [];

        collectedGold: number = 0;
        killedMonsters: number = 0;

        enemySpawner: EnemySpawner;

        collisionDetector: CollisionDetector;

        pathfinder: PathFinding;

        init(selectedHead: string, selectedArmor: string, selectedWeapon: string, selectedShield: string) {
            this.selectedHead = selectedHead || 'Hood';
            this.selectedArmor = selectedArmor || 'Cloth';
            this.selectedWeapon = selectedWeapon || 'Buckler';
            this.selectedShield = selectedShield || 'Rod';
        }

        preload() {
            this.game.stage.backgroundColor = '#243136';

            // For FPS
            this.game.time.advancedTiming = true;

            this.game.plugins.add(new Phaser.Plugin.Isometric(this.game));

            // Set the world size
            this.game.world.setBounds(0, 0, 3840, 1920);

            this.game.iso.anchor.set(0.5, 0.05);

            this.game.physics.startSystem(Phaser.Plugin.Isometric.ISOARCADE);

            // Global Gravity
            this.game.physics.isoArcade.gravity.setTo(0, 0, -500);
        }

        create() {
            this.enemies = [];

            // Map objects
            let levelMap = this.game.add.isoSprite(0, 0, 0, 'bg', 0);
            levelMap.anchor.set(0.5, 0);
            levelMap.scale.set(0.9, 0.9);

            this.mapManager = new MapManager(this.game);

            let trees = new MapFloorConfig("level1_1", "set1", 64, 64, 0, 0);

            this.mapManager.addFloor(trees, 48, 48, 48, 64, 0, 0, -48);

            // Creating AI
            this.pathfinder = new PathFinding(this.mapManager);

            this.hero = new Hero(this.game, this.mapManager.floorGroup, this.selectedHead,
                this.selectedArmor, this.selectedWeapon, this.selectedShield);

            this.game.camera.follow(this.hero.states[0]);

            this.userInterface = new UIManager(this.game, this.hero);

            this.enemySpawner = new EnemySpawner(this.game, this.enemies, this.mapManager);

            this.collisionDetector = new CollisionDetector(this.game, this.enemies, this.mapManager, this.pickUpItems, this.hero);

            this.collectedGold = 0;
            this.killedMonsters = 0;
        }

        update() {
            if (!this.userInterface.isPaused) {
                if (this.enemySpawner.spawnTimer.paused) {
                    this.enemySpawner.spawnTimer.resume();
                }
                this.hero.update();

                this.pathfinder.currentHeroXTile = Math.floor(this.hero.states[0].body.position.x / 32);
                this.pathfinder.currentHeroYtile = Math.floor(this.hero.states[0].body.position.y / 32);

                this.hero.spellCollection.forEach(function (spell) {
                    if (spell.states.alive) {
                        spell.update();
                    }
                    else {
                        this.hero.spellCollection.splice(this.hero.spellCollection.indexOf(spell), 1);
                        spell.states.destroy();
                    }
                }, this);

                this.enemies.forEach(function (enemy) {
                    if (enemy.states.alive) {
                        if (this.hero.currentState != UnitStates.DYING) {
                            this.pathfinder.pathfinding(enemy);
                        }
                        else {
                            enemy.update("idle");
                        }
                    }
                    else {
                        enemy.itemDrop(this.pickUpItems);
                        this.killedMonsters += 1;
                        this.enemies.splice(this.enemies.indexOf(enemy), 1);
                        enemy.states.destroy();
                    }
                    enemy.spellCollection.forEach(function (spell) {
                        if (spell.states.alive) {
                            spell.update();
                        }
                        else {
                            enemy.spellCollection.splice(enemy.spellCollection.indexOf(spell), 1);
                            spell.states.destroy();
                        }
                    }, this);
                }, this);

                if (!this.hero.states[0].alive) {
                    this.toGameOverState();
                    this.enemySpawner.spawnTimer.stop();
                }
            }
            else {
                this.enemySpawner.spawnTimer.pause();
            }

            this.userInterface.update();

            this.collisionDetector.collisionChecks();

            this.game.iso.topologicalSort(this.mapManager.floorGroup);
        }

        render() {
            new Renderer(this.game, this.enemies, this.mapManager, this.hero, this.pickUpItems);
        }

        toGameOverState() {
            this.game.state.start("GameOver", true, false,
                this.collectedGold,
                this.killedMonsters);
        }
    }
}