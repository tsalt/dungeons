///<reference path="Lib/phaser/phaser.d.ts"/>
///<reference path="States/Boot.ts"/>
///<reference path="States/Preload.ts"/>
///<reference path="States/MainMenu.ts"/>
///<reference path="States/Customization.ts"/>
///<reference path="States/Game.ts"/>
///<reference path="States/ArenaStage.ts"/>
///<reference path="States/GameOver.ts"/>

module Dungeons {
    class Dungeons {
        game: Phaser.Game;

        constructor(width?:number, height?:number) {
            this.game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-div', {create: this.create});
        }

        create() {
            this.game.state.add("Boot", Boot, true);
            this.game.state.add("Preload", Preload, true);
            this.game.state.add("MainMenu", MainMenu, true);
            this.game.state.add("Customization", Customization, true);
            this.game.state.add("GameOver", GameOver, true);
            this.game.state.add("Game", Game, true);
            this.game.state.add("ArenaStage", ArenaStage, true);

            this.game.state.start("Boot");
        }
    }

    window.onload = () => {
        new Dungeons(1280,720);
    };
}
